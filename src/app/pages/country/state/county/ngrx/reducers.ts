import { Action, createReducer, on, createFeatureSelector, createSelector } from '@ngrx/store';
import { PlaceModel } from '../../../../../models/place.model';
import * as CountyActions from './actions';

export const featureKey = 'county';
export const initialState: State = {};

export interface State {
  county?: PlaceModel;
  cities?: PlaceModel[];
}

const stateReducer = createReducer(
  initialState,
  on(CountyActions.loadCountySuccess, (state, { county }) => ({ ...state, county })),
  on(CountyActions.loadCountySymptomMetricSuccess, (state, { county, metric }) => ({ ...state, county: { ...county, metric } })),
  on(CountyActions.loadCitiesSuccess, (state, { cities }) => ({ ...state, cities })),
  on(CountyActions.loadSymptomMetricSuccess, (state, { city, metric }) => {
    const cities = [...state.cities];
    const index = cities.findIndex(c => c.placeId === city.placeId);

    if (index !== -1) {
      cities[index] = { ...city, metric };
    }

    return { ...state, cities };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectCountyFeature = createFeatureSelector<State>(featureKey);
export const selectCounty = createSelector(
  selectCountyFeature,
  (state: State) => state.county
);
export const selectCities = createSelector(
  selectCountyFeature,
  (state: State) => state.cities
);
