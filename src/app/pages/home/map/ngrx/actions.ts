import { createAction, props } from '@ngrx/store';
import { TemperatureModel } from '../../../../models/temperature.model';
import { TemperatureRange } from '../../../../exports/temperature.exports';
import { TemperatureFilterParams } from '../../../../services/http/temperature.service';
import { PlaceModel } from '../../../../models/place.model';

export const initialize = createAction('[Map Page] Initialize');
export const loadTemperatures = createAction('[Map Page] Load Temperatures', props<{ params: TemperatureFilterParams; }>());
export const loadTemperaturesSuccess = createAction('[Map Page] Load Temperatures Success', props<{ temperatures: TemperatureModel[]; }>());
export const updateShowHeatMap = createAction('[Map Page] Update Show Heatmap', props<{ showHeatMap: boolean; }>());
export const updateTemperatureFilter = createAction('[Map Page] Update Temperature Filter', props<{ range: TemperatureRange; }>());
export const updateReverseGeocode = createAction('[Map Page] Update Reverse Geocode', props<{ reverseGeocode: PlaceModel; }>());
