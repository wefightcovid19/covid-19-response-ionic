import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PlaceModel } from '../../../../../models/place.model';
import { SymptomModel } from '../../../../../models/symptom.model';
import { BadgeColorService } from '../../../../../services/badge-color.service';
import { TEMPERATURE_SYMPTOM } from '../../../../../exports/symptoms.exports';
import * as symptomReducers from '../../../../../ngrx/symptom/reducers';
import * as reducers from './ngrx/reducers';
import * as Actions from './ngrx/actions';

@Component({
  selector: 'app-city',
  templateUrl: './city.page.html',
  styleUrls: ['./city.page.scss'],
})
export class CityPage {

  readonly TEMPERATURE_SYMPTOM = TEMPERATURE_SYMPTOM;

  city$: Observable<PlaceModel>;
  neighborhoods$: Observable<PlaceModel[]>;
  symptoms$: Observable<SymptomModel[]>;
  selected$: Observable<SymptomModel>;

  constructor(
    private store$: Store<reducers.State>,
    private route: ActivatedRoute,
    public badgeColorService: BadgeColorService
  ) {
    this.city$ = this.store$.pipe(select(reducers.selectCity));
    this.neighborhoods$ = this.store$.pipe(select(reducers.selectNeighborhoods));
    this.symptoms$ = this.store$.pipe(select(symptomReducers.selectSymptoms));
    this.selected$ = this.store$.pipe(select(symptomReducers.selectSelected));
  }

  ionViewWillEnter() {
    this.route.paramMap.subscribe(params => {
      const cityId = params.get('cityId');
      this.store$.dispatch(Actions.loadCity({ cityId }));
      this.store$.dispatch(Actions.loadNeighborhoods({ cityId }));
    });
  }

  selectSymptom(symptom: SymptomModel) {
    this.store$.dispatch(Actions.selectSymptom({ symptom }));
  }
}
