import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationsMapComponent } from './locations-map.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    LocationsMapComponent
  ],
  declarations: [
    LocationsMapComponent
  ]
})
export class LocationsMapModule { }
