import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, first, tap, share } from 'rxjs/operators';
import { AppState } from '../ngrx/reducers';
import * as AuthActions from '../ngrx/auth/actions';

@Injectable({ providedIn: 'root' })
export class RefreshService {

  inFlightRefresh: Observable<boolean>;

  constructor(
    private store$: Store<AppState>,
    private actions$: Actions
  ) { }

  refresh(redirect = true): Observable<boolean> {
    if (!this.inFlightRefresh) {
      this.inFlightRefresh = this.actions$.pipe(
        ofType(AuthActions.refreshSuccess, AuthActions.refreshFailure),
        tap(() => this.inFlightRefresh = null),
        map(({ type }) => type === AuthActions.refreshSuccess.type),
        first(),
        share()
      );
      this.store$.dispatch(AuthActions.refresh({ redirect }));
      this.inFlightRefresh.toPromise();
    }

    return this.inFlightRefresh;
  }
}
