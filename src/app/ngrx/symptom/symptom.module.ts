import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SymptomEffects } from './effects';
import { reducer, featureKey } from './reducers';

@NgModule({
  imports: [
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([
      SymptomEffects
    ])
  ]
})
export class SymptomStoreModule { }
