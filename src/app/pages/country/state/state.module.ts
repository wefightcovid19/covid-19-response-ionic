import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StateEffects } from './ngrx/effects';
import { LocationsMapModule } from '../../../components/locations-map/locations-map.module';
import { SymptomSelectModule } from '../../../components/symptom-select/symptom-select.module';
import { StatePageRoutingModule } from './state-routing.module';
import { StatePage } from './state.page';
import { featureKey, reducer } from './ngrx/reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationsMapModule,
    SymptomSelectModule,
    StatePageRoutingModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([StateEffects])
  ],
  declarations: [
    StatePage
  ]
})
export class StatePageModule { }
