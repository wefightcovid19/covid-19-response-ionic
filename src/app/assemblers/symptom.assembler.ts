import { Injectable } from '@angular/core';
import { SymptomDto } from '../dtos/symptom.dto';

@Injectable({
  providedIn: 'root'
})
export class SymptomAssembler {

  assembleSymptomDto(dto: SymptomDto): SymptomDto {
    return { ...dto };
  }
}
