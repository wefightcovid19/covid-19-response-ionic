export class ErrorModel {
    message: string;
    detail?: string;
}
