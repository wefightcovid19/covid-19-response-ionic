import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumericDirective } from './numeric.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    NumericDirective
  ],
  declarations: [
    NumericDirective
  ]
})
export class DirectiveModule { }
