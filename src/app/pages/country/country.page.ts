import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PlaceModel } from '../../models/place.model';
import { BadgeColorService } from '../../services/badge-color.service';
import { SymptomModel } from '../../models/symptom.model';
import { TEMPERATURE_SYMPTOM } from '../../exports/symptoms.exports';
import * as symptomReducers from '../../ngrx/symptom/reducers';
import * as reducers from './ngrx/reducers';
import * as Actions from './ngrx/actions';

@Component({
  selector: 'app-country',
  templateUrl: './country.page.html',
  styleUrls: ['./country.page.scss'],
})
export class CountryPage {

  readonly TEMPERATURE_SYMPTOM = TEMPERATURE_SYMPTOM;

  country$: Observable<PlaceModel>;
  states$: Observable<PlaceModel[]>;
  symptoms$: Observable<SymptomModel[]>;
  selected$: Observable<SymptomModel>;

  constructor(
    private store$: Store<reducers.State>,
    public badgeColorService: BadgeColorService
  ) {
    this.country$ = this.store$.pipe(select(reducers.selectCountry));
    this.states$ = this.store$.pipe(select(reducers.selectStates));
    this.symptoms$ = this.store$.pipe(select(symptomReducers.selectSymptoms));
    this.selected$ = this.store$.pipe(select(symptomReducers.selectSelected));
  }

  ionViewWillEnter() {
    this.store$.dispatch(Actions.loadStates());
    this.store$.dispatch(Actions.loadUnitedStates());
  }

  selectSymptom(symptom: SymptomModel) {
    this.store$.dispatch(Actions.selectSymptom({ symptom }));
  }
}
