import { Injectable } from '@angular/core';
import { AlertController, ToastController, NavController } from '@ionic/angular';
import { AlertOptions, AlertInput } from '@ionic/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of, timer, from } from 'rxjs';
import { map, switchMap, catchError, withLatestFrom, tap } from 'rxjs/operators';
import { ProfileModel } from '../../../../models/profile.model';
import { PlaceService } from '../../../../services/http/place.service';
import { ConstraintValidationErrorModel } from '../../../../models/error/constraint-validation-error.model';
import { ProfileService } from '../../../../services/http/profile.service';
import { DEFAULT_TOAST_OPTIONS } from '../../../../exports/toast.exports';
import * as ProcessingIndicatorActions from '../../../../ngrx/processing-indicator/actions';
import * as ProfileActions from './actions';
import * as reducers from './reducers';
import * as AuthActions from '../../../../ngrx/auth/actions';
import * as authReducers from '../../../../ngrx/auth/reducers';

const GEOLOCATION_TIMEOUT = 30 * 1000;
const GEOLOCATION_MAXIMUM_AGE = 5 * 60 * 1000;

@Injectable({ providedIn: 'root' })
export class ProfileEffects {

  alert: HTMLIonAlertElement;

  constructor(
    private store$: Store<reducers.State>,
    private actions$: Actions,
    private alertController: AlertController,
    private toastController: ToastController,
    private navController: NavController,
    private placeService: PlaceService,
    private profileService: ProfileService,
    private geolocation: Geolocation
  ) { }

  initialize$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.initialize, ProfileActions.updateProfileError),
    withLatestFrom(this.store$.pipe(select(authReducers.selectProfile))),
    map(([, profile]) => ProfileActions.updateProfileInitialize({ profile }))
  ));

  updateProfile$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.updateProfile),
    withLatestFrom(this.store$.pipe(select(authReducers.selectProfile))),
    withLatestFrom(this.store$.pipe(select(reducers.selectProfile))),
    switchMap(([[, authProfile], profile]) => {
      let operation = this.profileService.postProfile(profile);

      if (authProfile) {
        operation = this.profileService.putProfile(profile);
      }

      return operation.pipe(
        map(result => ProfileActions.updateProfileSuccess({ profile: result }))
      );
    })
  ));

  updateProfileSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.updateProfileSuccess),
    tap(() => { this.navController.back(); }),
    map(profile => AuthActions.loadProfileSuccess(profile))
  ));

  alertUpdateLocation$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.alertUpdateProfileLocation),
    tap(() => {
      const waitFor = [ProfileActions.updateProfilePossibleLocationsSuccess.type];
      this.store$.dispatch(ProcessingIndicatorActions.showLoading({ waitFor }));
    }),
    switchMap(() => from(this.geolocation.getCurrentPosition({ timeout: GEOLOCATION_TIMEOUT, maximumAge: GEOLOCATION_MAXIMUM_AGE })).pipe(
      tap(({ coords }) => this.store$.dispatch(ProfileActions.updateProfilePossibleLocations({ coords }))),
      switchMap(position => this.placeService.geocode(position)),
      map(locations => ProfileActions.updateProfilePossibleLocationsSuccess({ locations })),
      catchError(error => of(ProfileActions.updateProfilePossibleLocationsError(error)))
    ))
  ));

  updatePossibleLocationsSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.updateProfilePossibleLocationsSuccess),
    tap(({ type: actionType }) => this.store$.dispatch(ProcessingIndicatorActions.removeWaitingFor({ actionType }))),
    withLatestFrom(this.store$.pipe(select(reducers.selectProfile))),
    tap(async ([{ locations }, profile]) => {
      const inputs = locations.map(({ formattedAddress }) => ({
        type: 'radio',
        label: formattedAddress,
        value: formattedAddress
      })) as AlertInput[];
      inputs[0].checked = true;

      const options: AlertOptions = {
        header: 'My Location',
        inputs,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Update',
            handler: formattedAddress => {
              const model: ProfileModel = { ...profile, reverseGeocode: { ...profile.reverseGeocode, formattedAddress } };
              this.store$.dispatch(ProfileActions.updateProfileLocation({ profile: model }));
            }
          }
        ]
      };

      const alert = await this.alertController.create(options);
      alert.present();
    })
  ), { dispatch: false });

  updatePossibleLocationsError$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.updateProfilePossibleLocationsError),
    tap(() => timer(500).toPromise().then(() => this.store$.dispatch(ProcessingIndicatorActions.reset()))),
    tap(async ({ error }) => {
      let message = 'An error has occurred while finding your location';

      if (error && error.code === 1) {
        message = 'Permission to find you location has been denied';
      }

      const options = { ...DEFAULT_TOAST_OPTIONS, message };
      const toast = await this.toastController.create(options);

      toast.present();
    })
  ), { dispatch: false });

  profileUpdateError$ = createEffect(() => this.actions$.pipe(
    ofType(ProfileActions.updateProfileError),
    tap(() => this.store$.dispatch(ProcessingIndicatorActions.reset())),
    tap(async ({ error }) => {
      if (error instanceof ConstraintValidationErrorModel) {
        const options: AlertOptions = {
          header: 'Profile Update Error',
          message: error.violations[0],
          buttons: [
            {
              text: 'Ok',
              role: 'cancel'
            }
          ]
        };

        const alert = await this.alertController.create(options);
        alert.present();
      }
    })
  ), { dispatch: false });
}
