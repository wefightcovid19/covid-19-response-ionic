import { ActionReducerMap, MetaReducer, createFeatureSelector } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { environment } from '../../environments/environment';

export interface AppState {
  router: fromRouter.RouterReducerState<any>;
}

export const selectRouter = createFeatureSelector<AppState, fromRouter.RouterReducerState<any>>('router');

export const { selectUrl } = fromRouter.getSelectors(selectRouter);

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
