export interface PlaceDto {
  placeId: string;
  googleMapsPlaceId: string;
  formattedAddress: string;
  northBound: number;
  eastBound: number;
  southBound: number;
  westBound: number;
  latitude: number;
  longitude: number;
}
