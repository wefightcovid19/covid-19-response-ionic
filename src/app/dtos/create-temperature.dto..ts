import { TemperatureType } from '../exports/symptoms.exports';
import { CreateReverseGeocodeDto } from './create-reverse-geocode.dto';
import { SymptomDto } from './symptom.dto';

export interface CreateTemperatureDto {
  temperature?: number;
  type?: TemperatureType;
  symptons?: SymptomDto[];
  createReverseGeocode?: CreateReverseGeocodeDto;
}
