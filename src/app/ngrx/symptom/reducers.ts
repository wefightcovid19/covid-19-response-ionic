import { Action, createSelector, createFeatureSelector, createReducer, on } from '@ngrx/store';
import { SymptomModel } from '../../models/symptom.model';
import { NO_SYMPTOMS } from '../../exports/symptoms.exports';
import * as SymptomsActions from './actions';

export const featureKey = 'symptom';
export const initialState: State = {};

export interface State {
  symptoms?: SymptomModel[];
  selected?: SymptomModel;
}

const stateReducer = createReducer(
  initialState,
  on(SymptomsActions.getSymptomsSuccess, (state, { symptoms }) => {
    const index = symptoms.findIndex(symptom => symptom.id === NO_SYMPTOMS);
    symptoms = [...symptoms];
    symptoms.splice(index, 1);

    return { ...state, symptoms };
  }),
  on(SymptomsActions.selectSymptom, (state, { selected }) => ({ ...state, selected }))
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectSymptomFeature = createFeatureSelector<State>(featureKey);
export const selectSymptoms = createSelector(
  selectSymptomFeature,
  (state: State) => state.symptoms
);
export const selectSelected = createSelector(
  selectSymptomFeature,
  (state: State) => state.selected
);
