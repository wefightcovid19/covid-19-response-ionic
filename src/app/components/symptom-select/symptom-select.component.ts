import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SymptomModel } from '../../models/symptom.model';

@Component({
  selector: 'app-symptom-select',
  templateUrl: './symptom-select.component.html',
  styleUrls: ['./symptom-select.component.scss'],
})
export class SymptomSelectComponent {

  readonly customActionSheetOptions = {
    header: 'Select a symptom to explore'
  };

  @Input()
  selected: SymptomModel;

  @Input()
  symptoms: SymptomModel[];

  @Output()
  select = new EventEmitter<SymptomModel>();
}
