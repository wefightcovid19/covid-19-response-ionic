import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CountryPage } from './country.page';

const routes: Routes = [
  {
    path: '',
    component: CountryPage
  },
  {
    path: ':stateId',
    loadChildren: () => import('./state/state.module').then( m => m.StatePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CountryPageRoutingModule { }
