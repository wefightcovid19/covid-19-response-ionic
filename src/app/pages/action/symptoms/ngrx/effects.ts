import { DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { AlertController, ToastController, NavController } from '@ionic/angular';
import { AlertOptions } from '@ionic/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { withLatestFrom, tap, switchMap, map } from 'rxjs/operators';
import { TemperatureService } from '../../../../services/http/temperature.service';
import { DEFAULT_TOAST_OPTIONS } from '../../../../exports/toast.exports';
import * as MapActions from '../../../home/map/ngrx/actions';
import * as mapReducers from '../../../home/map/ngrx/reducers';
import * as SymptomsActions from './actions';
import * as reducers from './reducers';

const GEOLOCATION_TIMEOUT = 30 * 1000;
const GEOLOCATION_MAXIMUM_AGE = 5 * 60 * 1000;

@Injectable({ providedIn: 'root' })
export class SymptomsEffects {

  reportTemperatureAlert: HTMLIonAlertElement;

  constructor(
    private store$: Store<reducers.State>,
    private actions$: Actions,
    private navController: NavController,
    private alertController: AlertController,
    private toastController: ToastController,
    private temperatureService: TemperatureService,
    private geolocation: Geolocation,
    private decimalPipe: DecimalPipe
  ) { }

  submitTemperature$ = createEffect(() => this.actions$.pipe(
    ofType(SymptomsActions.submitSymptoms),
    withLatestFrom(this.store$.pipe(select(reducers.selectCreateTemperature))),
    switchMap(([, createTemperature]) => this.temperatureService.postTemperature(createTemperature).pipe(
      map(() => SymptomsActions.submitSymptomsSuccess())
    ))
  ));

  submitTemperatureSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(SymptomsActions.submitSymptomsSuccess),
    tap(() => { this.navController.back(); }),
    switchMap(() => this.actions$.pipe(
      ofType(MapActions.loadTemperaturesSuccess),
      withLatestFrom(this.store$.pipe(select(mapReducers.selectRange))),
      tap(async () => {
        const message = 'Symptoms reported';
        const toastOptions = { ...DEFAULT_TOAST_OPTIONS, message };
        const toast = await this.toastController.create(toastOptions);

        toast.present();
      }),
      map(([, range]) => MapActions.updateTemperatureFilter({ range }))
    ))
  ));

  alertSubmitTemperature$ = createEffect(() => this.actions$.pipe(
    ofType(SymptomsActions.alertSubmitSymptoms),
    withLatestFrom(this.store$.pipe(select(reducers.selectCreateTemperature))),
    tap(async ([, { temperature, symptoms }]) => {
      let symptomList = '<ul>';
      symptoms.forEach(({ name }) => { symptomList += `<li>${name}</li>`; });
      symptomList += '</ul>';

      const options: AlertOptions = {
        header: 'Report Symptoms',
        message: `Please use the checkbox below to report your location along with the following symptoms: ${symptomList}`,
        cssClass: 'emphasizeTemperature',
        inputs: [{
          label: 'Loading...',
          name: 'sendLocation',
          type: 'checkbox',
          checked: false,
          disabled: true
        }],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          }, {
            text: 'Report',
            handler: (data: any[]) => {
              if (data.length > 0) {
                const { latitude, longitude } = data[0] as Coordinates;
                this.store$.dispatch(SymptomsActions.updateLocation({ latitude, longitude }));
              }
              this.store$.dispatch(SymptomsActions.submitSymptoms());
            }
          }
        ]
      };

      if (temperature) {
        const formattedTemperature = this.decimalPipe.transform(temperature);
        options.message = `Please use the checkbox below to report your location along with the
        temperature of <b>${formattedTemperature}&deg;</b> and the following symptoms: ${symptomList}`;
      }

      this.reportTemperatureAlert = await this.alertController.create(options);

      await this.reportTemperatureAlert.present();
      this.geolocation.getCurrentPosition({ timeout: GEOLOCATION_TIMEOUT, maximumAge: GEOLOCATION_MAXIMUM_AGE })
        .then(position => {
          this.reportTemperatureAlert.inputs = [{
            label: 'Report location',
            name: 'sendLocation',
            type: 'checkbox',
            checked: true,
            value: position.coords
          }];
        })
        .catch(() => {
          this.reportTemperatureAlert.inputs = [{
            label: 'Location information unavailable',
            name: 'sendLocation',
            type: 'checkbox',
            checked: false,
            disabled: true
          }];
        });
    })
  ), { dispatch: false });
}
