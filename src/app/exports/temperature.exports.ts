export interface TemperatureRange {
  lower: number;
  upper: number;
}
