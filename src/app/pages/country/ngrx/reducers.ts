import { Action, createReducer, on, createFeatureSelector, createSelector } from '@ngrx/store';
import { PlaceModel } from '../../../models/place.model';
import * as StateActions from './actions';

export const featureKey = 'country';
export const initialState: State = {};

export interface State {
  country?: PlaceModel;
  states?: PlaceModel[];
}

const stateReducer = createReducer(
  initialState,
  on(StateActions.loadUnitedStatesSuccess, (state, { country }) => ({ ...state, country })),
  on(StateActions.loadStatesSuccess, (state, { states }) => ({ ...state, states })),
  on(StateActions.loadSymptomMetricSuccess, (state, result) => {
    const states = [...state.states];
    const index = states.findIndex(s => s.placeId === result.state.placeId);

    if (index !== -1) {
      states[index] = { ...result.state, metric: result.metric };
    }

    return { ...state, states };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectStateFeature = createFeatureSelector<State>(featureKey);
export const selectStates = createSelector(
  selectStateFeature,
  (state: State) => state.states
);
export const selectCountry = createSelector(
  selectStateFeature,
  (state: State) => state.country
);
