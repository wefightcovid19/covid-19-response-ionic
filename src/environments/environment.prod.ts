export const environment = {
  production: true,
  name: 'production',
  authBasePath: 'https://www.app.wefightcovid19.online/auth',
  temperatureTrackerBasePath: 'https://www.app.wefightcovid19.online/temperature-tracker',
  socialProviderRedirectUri: 'https://www.wefightcovid19.online/home/map'
};
