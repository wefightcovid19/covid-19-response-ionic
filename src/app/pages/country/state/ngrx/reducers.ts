import { Action, createReducer, on, createFeatureSelector, createSelector } from '@ngrx/store';
import { PlaceModel } from '../../../../models/place.model';
import * as StateActions from './actions';

export const featureKey = 'state';
export const initialState: State = {};

export interface State {
  state?: PlaceModel;
  counties?: PlaceModel[];
}

const stateReducer = createReducer(
  initialState,
  on(StateActions.loadStateSuccess, (s, { state }) => ({ ...s, state })),
  on(StateActions.loadStateSymptomMetricSuccess, (s, { state, metric }) => ({ ...s, state: { ...state, metric } })),
  on(StateActions.loadCountiesSuccess, (state, { counties }) => ({ ...state, counties })),
  on(StateActions.loadSymptomMetricSuccess, (state, { county, metric }) => {
    const counties = [...state.counties];
    const index = counties.findIndex(c => c.placeId === county.placeId);

    if (index !== -1) {
      counties[index] = { ...county, metric };
    }

    return { ...state, counties };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectStateDetailFeature = createFeatureSelector<State>(featureKey);
export const selectState = createSelector(
  selectStateDetailFeature,
  (state: State) => state.state
);
export const selectCounties = createSelector(
  selectStateDetailFeature,
  (state: State) => state.counties
);
