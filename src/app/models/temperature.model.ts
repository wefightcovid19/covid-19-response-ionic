import { TemperatureType } from '../exports/symptoms.exports';

export class TemperatureModel {
  temperature: number;
  type: TemperatureType;
  latitude: number;
  longitude: number;
}
