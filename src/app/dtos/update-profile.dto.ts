import { GenderType } from '../exports/gender.export';
import { CreateReverseGeocodeDto } from './create-reverse-geocode.dto';
import { TemperatureType } from '../exports/symptoms.exports';

export interface UpdateProfileDto {
  age: number;
  gender: GenderType;
  temperatureUnit: TemperatureType;
  createReverseGeocode: CreateReverseGeocodeDto;
}
