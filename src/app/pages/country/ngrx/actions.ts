import { createAction, props } from '@ngrx/store';
import { PlaceModel } from '../../../models/place.model';
import { SymptomModel } from '../../../models/symptom.model';

export const loadUnitedStates = createAction('[Country Page] Load United States');
export const loadUnitedStatesSuccess = createAction('[Country Page] Load United States Success', props<{ country: PlaceModel; }>());
export const loadStates = createAction('[Country Page] Load States');
export const loadStatesSuccess = createAction('[Country Page] Load States Success', props<{ states: PlaceModel[]; }>());
export const loadSymptomMetric = createAction(
  '[Country Page] Load Symptom Metric',
  props<{ state: PlaceModel; symptom: SymptomModel; }>()
);
export const loadSymptomMetricSuccess = createAction(
  '[Country Page] Load Symptom Metric Success',
  props<{ metric: number; state: PlaceModel; }>()
);
export const selectSymptom = createAction('[Country Page] Select Symptom', props<{ symptom: SymptomModel }>());
