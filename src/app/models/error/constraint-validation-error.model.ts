import { ErrorModel } from './error.model';

export class ConstraintValidationErrorModel extends ErrorModel {
    violations: string[];
}
