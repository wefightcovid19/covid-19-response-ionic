export interface CreateReverseGeocodeDto {
  latitude: number;
  longitude: number;
}
