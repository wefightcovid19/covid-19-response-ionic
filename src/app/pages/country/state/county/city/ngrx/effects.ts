import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';
import { PlaceService } from '../../../../../../services/http/place.service';
import { SymptomService } from '../../../../../../services/http/symptom.service';
import { TemperatureService, TemperatureFilterParams } from '../../../../../../services/http/temperature.service';
import { TEMPERATURE_SYMPTOM } from '../../../../../../exports/symptoms.exports';
import * as SymptomActions from '../../../../../../ngrx/symptom/actions';
import * as symptomReducers from '../../../../../../ngrx/symptom/reducers';
import * as CityActions from './actions';
import * as cityReducers from './reducers';

@Injectable({
  providedIn: 'root'
})
export class CityEffects {

  constructor(
    private store$: Store<cityReducers.State>,
    private actions$: Actions,
    private placeService: PlaceService,
    private temperatureService: TemperatureService,
    private symptomService: SymptomService
  ) { }

  loadCity$ = createEffect(() => this.actions$.pipe(
    ofType(CityActions.loadCity),
    map(action => action.cityId),
    mergeMap(countyId => this.placeService.place(countyId).pipe(
      map(city => CityActions.loadCitySuccess({ city }))
    ))
  ));

  loadCitySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CityActions.loadCitySuccess),
    mergeMap(({ city }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: city.northBound,
        east: city.eastBound,
        south: city.southBound,
        west: city.westBound
      };

      return this.temperatureService.getAverageTemperature(params).pipe(
        map(metric => CityActions.loadCitySymptomMetricSuccess({ metric, city }))
      );
    })
  ));

  loadNeighborhoods$ = createEffect(() => this.actions$.pipe(
    ofType(CityActions.loadNeighborhoods),
    map(action => action.cityId),
    mergeMap(cityId => this.placeService.children(cityId).pipe(
      map(neighborhoods => CityActions.loadNeighborhoodsSuccess({ neighborhoods }))
    ))
  ));

  loadNeighborhoodsSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CityActions.loadNeighborhoodsSuccess),
    withLatestFrom(this.store$.pipe(select(symptomReducers.selectSelected))),
    mergeMap(([{ neighborhoods }, symptom]) => neighborhoods.map(neighborhood => CityActions.loadSymptomMetric({ neighborhood, symptom })))
  ));

  loadSymptomMetric$ = createEffect(() => this.actions$.pipe(
    ofType(CityActions.loadSymptomMetric),
    mergeMap(({ neighborhood, symptom }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: neighborhood.northBound,
        east: neighborhood.eastBound,
        south: neighborhood.southBound,
        west: neighborhood.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => CityActions.loadSymptomMetricSuccess({ metric, neighborhood })));
    })
  ));

  selectSymptom$ = createEffect(() => this.actions$.pipe(
    ofType(CityActions.selectSymptom),
    tap(({ symptom }) => this.store$.dispatch(SymptomActions.selectSymptom({ selected: symptom }))),
    withLatestFrom(this.store$.pipe(select(cityReducers.selectNeighborhoods))),
    mergeMap(([{ symptom }, neighborhoods]) => forkJoin(neighborhoods.map(neighborhood => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: neighborhood.northBound,
        east: neighborhood.eastBound,
        south: neighborhood.southBound,
        west: neighborhood.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => CityActions.loadSymptomMetricSuccess({ metric, neighborhood })));
    }))),
    mergeMap(results => results)
  ));
}
