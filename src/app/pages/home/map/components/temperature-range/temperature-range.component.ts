import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonRange } from '@ionic/angular';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { TemperatureRange } from '../../../../../exports/temperature.exports';
import * as MapActions from '../../ngrx/actions';
import * as MapReducers from '../../ngrx/reducers';

const min = 99;
const max = 104;

@Component({
  selector: 'app-temperature-range',
  templateUrl: './temperature-range.component.html',
  styleUrls: ['./temperature-range.component.scss'],
})
export class TemperatureRangeComponent implements OnInit {

  @ViewChild(IonRange, { static: true })
  range: IonRange;

  readonly rangeMin = min * 10;
  readonly rangeMax = max * 10;
  lower = this.rangeMin;
  upper = this.rangeMax;

  constructor(
    private store$: Store<MapReducers.State>,
    private actions$: Actions
  ) { }

  ngOnInit() {
    this.range.value = ({
      lower: this.lower,
      upper: this.upper
    });
    this.actions$.pipe(
      ofType(MapActions.loadTemperaturesSuccess),
      take(1)
    ).toPromise().then(() => this.updateFilteredTemperatures(this.range.value as any));
  }

  @Input()
  set temperatureRange(range: TemperatureRange) {
    if (range) {
      this.lower = range.lower;
      this.upper = range.upper;
    }
  }

  updateFilteredTemperatures(range: TemperatureRange) {
    this.store$.dispatch(MapActions.updateTemperatureFilter({ range }));
    this.temperatureRange = range;
  }
}
