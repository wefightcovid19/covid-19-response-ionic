import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LocationsMapModule } from '../../../../../components/locations-map/locations-map.module';
import { SymptomSelectModule } from '../../../../../components/symptom-select/symptom-select.module';
import { CityPageRoutingModule } from './city-routing.module';
import { CityPage } from './city.page';
import { CityEffects } from './ngrx/effects';
import { featureKey, reducer } from './ngrx/reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationsMapModule,
    SymptomSelectModule,
    CityPageRoutingModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([CityEffects])
  ],
  declarations: [
    CityPage
  ]
})
export class CityPageModule { }
