import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { map, mergeMap, withLatestFrom, tap } from 'rxjs/operators';
import { PlaceService } from '../../../services/http/place.service';
import { SymptomService } from '../../../services/http/symptom.service';
import { TemperatureService, TemperatureFilterParams } from '../../../services/http/temperature.service';
import { TEMPERATURE_SYMPTOM } from '../../../exports/symptoms.exports';
import { Country } from '../../../exports/place.export';
import * as SymptomActions from '../../../ngrx/symptom/actions';
import * as symptomReducers from '../../../ngrx/symptom/reducers';
import * as CountryActions from './actions';
import * as countryReducers from './reducers';

@Injectable({
  providedIn: 'root'
})
export class CountryEffects {

  constructor(
    private store$: Store<countryReducers.State>,
    private actions$: Actions,
    private placeService: PlaceService,
    private temperatureService: TemperatureService,
    private symptomService: SymptomService
  ) { }

  loadUnitedStates$ = createEffect(() => this.actions$.pipe(
    ofType(CountryActions.loadUnitedStates),
    mergeMap(() => this.placeService.place(Country.UNITED_STATES).pipe(
      map(country => CountryActions.loadUnitedStatesSuccess({ country }))
    ))
  ));

  loadStates$ = createEffect(() => this.actions$.pipe(
    ofType(CountryActions.loadStates),
    mergeMap(() => this.placeService.children(Country.UNITED_STATES).pipe(
      map(states => CountryActions.loadStatesSuccess({ states }))
    ))
  ));

  loadStatesSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CountryActions.loadStatesSuccess),
    withLatestFrom(this.store$.pipe(select(symptomReducers.selectSelected))),
    mergeMap(([{ states }, symptom]) => states.map(state => CountryActions.loadSymptomMetric({ state, symptom })))
  ));

  loadSymptomMetric$ = createEffect(() => this.actions$.pipe(
    ofType(CountryActions.loadSymptomMetric),
    mergeMap(({ state, symptom }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: state.northBound,
        east: state.eastBound,
        south: state.southBound,
        west: state.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => CountryActions.loadSymptomMetricSuccess({ metric, state })));
    })
  ));

  selectSymptom$ = createEffect(() => this.actions$.pipe(
    ofType(CountryActions.selectSymptom),
    tap(({ symptom }) => this.store$.dispatch(SymptomActions.selectSymptom({ selected: symptom }))),
    withLatestFrom(this.store$.pipe(select(countryReducers.selectStates))),
    mergeMap(([{ symptom }, states]) => forkJoin(states.map(state => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: state.northBound,
        east: state.eastBound,
        south: state.southBound,
        west: state.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => CountryActions.loadSymptomMetricSuccess({ metric, state })));
    }))),
    mergeMap(results => results)
  ));
}
