import { createAction, props } from '@ngrx/store';
import { ProfileModel } from '../../../../models/profile.model';
import { ConstraintValidationErrorModel } from '../../../../models/error/constraint-validation-error.model';
import { GenderType } from '../../../../exports/gender.export';
import { PlaceModel } from '../../../../models/place.model';
import { TemperatureType } from '../../../../exports/symptoms.exports';

export const initialize = createAction('[Update Profile] Initialize');
export const updateProfileInitialize = createAction('[Update Profile] Update Profile Initialize', props<{ profile: ProfileModel; }>());
export const updateProfile = createAction('[Update Profile] Update Profile');
export const updateProfileSuccess = createAction('[Update Profile] Update Profile Success', props<{ profile: ProfileModel; }>());
export const updateProfileGender = createAction('[Update Profile] Update Profile Gender', props<{ gender: GenderType; }>());
export const updateProfileTemperatureUnit = createAction(
  '[Update Profile] Update Profile Temperature Unit',
  props<{ temperatureUnit: TemperatureType; }>()
);
export const updateProfileAge = createAction('[Update Profile] Update Profile Age', props<{ age: number; }>());
export const alertUpdateProfileLocation = createAction('[Update Profile] Alert Update Profile Location');
export const updateProfileLocation = createAction('[Update Profile] Update Profile Location', props<{ profile: ProfileModel; }>());
export const updateProfileLocationSuccess = createAction(
  '[Update Profile] Update Profile Location Success',
  props<{ profile: ProfileModel; }>()
);
export const updateProfilePossibleLocations = createAction(
  '[Update Profile] Update Profile Possible Locations',
  props<{ coords: Coordinates; }>()
);
export const updateProfilePossibleLocationsSuccess = createAction(
  '[Update Profile] Update Profile Possible Locations Success',
  props<{ locations: PlaceModel[]; }>()
);
export const updateProfilePossibleLocationsError = createAction(
  '[Update Profile] Update Profile Possible Locations Error',
  props<{ error: any; }>()
);
export const updateProfileError = createAction(
  '[Update Profile] Update Profile Error',
  props<{ error: ConstraintValidationErrorModel; }>()
);
export const reset = createAction('[Update Profile] Reset');
