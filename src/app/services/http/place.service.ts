import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PlaceModel } from '../../models/place.model';
import { PlaceDto } from '../../dtos/place.dto';

@Injectable({ providedIn: 'root' })
export class PlaceService {

  constructor(
    private httpClient: HttpClient
  ) { }

  root(): Observable<PlaceModel[]> {
    const url = `${environment.temperatureTrackerBasePath}/place`;
    return this.httpClient.get<PlaceDto[]>(url);
  }

  place(id: string): Observable<PlaceModel> {
    const url = `${environment.temperatureTrackerBasePath}/place/${id}`;
    return this.httpClient.get<PlaceDto>(url);
  }

  children(id: string): Observable<PlaceModel[]> {
    const url = `${environment.temperatureTrackerBasePath}/place/${id}/children`;
    return this.httpClient.get<PlaceDto[]>(url);
  }

  geocode(position: Position): Observable<PlaceModel[]> {
    const { latitude, longitude } = position.coords;
    const url = `${environment.temperatureTrackerBasePath}/place/geocode/reverse?latitude=${latitude}&longitude=${longitude}`;
    return this.httpClient.get<PlaceDto[]>(url);
  }
}
