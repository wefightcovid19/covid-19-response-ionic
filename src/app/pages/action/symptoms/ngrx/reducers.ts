import { Action, createFeatureSelector, createSelector, on, createReducer } from '@ngrx/store';
import { CreateTemperatureModel } from '../../../../models/create-temperature.model';
import * as SymptomsActions from './actions';

export const featureKey = 'reportSymptoms';
const initialState: State = {
  createTemperature: {
    symptoms: []
  }
};

export interface State {
  createTemperature: CreateTemperatureModel;
}

const stateReducer = createReducer(
  initialState,
  on(SymptomsActions.updateTemperature, (state, { temperature }) => ({
    ...state,
    createTemperature: {
      ...state.createTemperature,
      temperature
    }
  })),
  on(SymptomsActions.updateTemperatureType, (state, { temperatureType }) => ({
    ...state,
    createTemperature: {
      ...state.createTemperature,
      type: temperatureType
    }
  })),
  on(SymptomsActions.updateLocation, (state, { latitude, longitude }) => ({
    ...state,
    createTemperature: {
      ...state.createTemperature,
      createReverseGeocode: {
        ...state.createTemperature.createReverseGeocode,
        latitude,
        longitude
      }
    }
  })),
  on(SymptomsActions.selectSymption, (state, { symptom }) => {
    return {
      ...state,
      createTemperature: {
        ...state.createTemperature,
        symptoms: [...state.createTemperature.symptoms, symptom]
      }
    };
  }),
  on(SymptomsActions.deselectSymption, (state, { symptom }) => {
    const symptoms = [...state.createTemperature.symptoms];
    const index = state.createTemperature.symptoms.findIndex(s => symptom.id === s.id);

    if (index !== -1) {
      symptoms.splice(index, 1);
    }

    return {
      ...state,
      createTemperature: { ...state.createTemperature, symptoms }
    };
  }),
  on(SymptomsActions.reset, () => ({ ...initialState }))
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

const selectTemperatureFeature = createFeatureSelector<State>(featureKey);
export const selectCreateTemperature = createSelector(
  selectTemperatureFeature,
  (state: State) => state.createTemperature
);
