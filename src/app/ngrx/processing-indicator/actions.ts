import { Action, createAction, props } from '@ngrx/store';

export const showLoading = createAction('[Processing Indicator] Show Loading', props<{ waitFor: string[]; }>());
export const removeWaitingFor = createAction('[Processing Indicator] Remove Waiting For', props<{ actionType: string; }>());
export const reset = createAction('[Processing Indicator] Reset');
