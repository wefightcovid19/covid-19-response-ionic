import { Action, createSelector, createFeatureSelector, createReducer, on } from '@ngrx/store';
import { LoginModel } from '../../models/login.model';
import { ProfileModel } from '../../models/profile.model';
import * as AuthActions from './actions';

export const featureKey = 'auth';
export const initialState: State = { refreshing: false };

export interface State {
  login?: LoginModel;
  profile?: ProfileModel;
  email?: string;
  refreshing: boolean;
}

const stateReducer = createReducer(
  initialState,
  on(AuthActions.updateLoginEmail, (state, { email }) => ({ ...state, login: { ...state.login, email } })),
  on(AuthActions.updateLoginPassword, (state, { password }) => ({ ...state, login: { ...state.login, password } })),
  on(AuthActions.loadEmail, (state, { email }) => ({ ...state, email })),
  on(AuthActions.loadProfileSuccess, (state, { profile }) => ({ ...state, profile: { ...state.profile, ...profile} })),
  on(AuthActions.refresh, (state) => ({ ...state, refreshing: true })),
  on(AuthActions.refreshSuccess, AuthActions.refreshFailure, (state) => ({ ...state, refreshing: false })),
  on(AuthActions.loginSuccess, (state) => ({ ...state, login: undefined })),
  on(AuthActions.reset, () => ({ ...initialState }))
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectAuth = createFeatureSelector<State>(featureKey);
export const selectLogin = createSelector(
  selectAuth,
  (state: State) => state.login
);
export const selectEmail = createSelector(
  selectAuth,
  (state: State) => state.email
);
export const selectProfile = createSelector(
  selectAuth,
  (state: State) => state.profile
);
export const selectRefreshing = createSelector(
  selectAuth,
  (state: State) => state.refreshing
);
