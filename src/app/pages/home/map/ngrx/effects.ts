import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { map, mergeMap, withLatestFrom, filter, flatMap } from 'rxjs/operators';
import { TemperatureService, TemperatureFilterParams } from '../../../../services/http/temperature.service';
import * as MapActions from './actions';
import * as mapReducers from './reducers';
import * as AuthActions from '../../../../ngrx/auth/actions';
import * as authReducers from '../../../../ngrx/auth/reducers';
import * as SymptomsActions from '../../../action/symptoms/ngrx/actions';

@Injectable({
  providedIn: 'root'
})
export class MapEffects {

  constructor(
    private actions$: Actions,
    private store$: Store<mapReducers.State>,
    private temperatureService: TemperatureService
  ) { }

  initialize$ = createEffect(() => this.actions$.pipe(
    ofType(MapActions.initialize),
    withLatestFrom(this.store$.pipe(select(authReducers.selectRefreshing))),
    flatMap(([, refreshing]) => {
      let selectProfile = this.store$.pipe(select(authReducers.selectProfile));

      if (refreshing) {
        selectProfile = this.actions$.pipe(ofType(AuthActions.loadProfileSuccess)).pipe(
          flatMap(() => this.store$.pipe(select(authReducers.selectProfile)))
        );
      }

      return selectProfile;
    }),
    filter(profile => !!profile),
    map(profile => MapActions.updateReverseGeocode({ reverseGeocode: profile.reverseGeocode }))
  ));

  loadTemperaturesWithReverseGeocode$ = createEffect(() => this.actions$.pipe(
    ofType(MapActions.updateReverseGeocode, SymptomsActions.submitSymptomsSuccess),
    withLatestFrom(this.store$.pipe(select(authReducers.selectProfile)).pipe(
      filter(profile => !!profile),
      map(profile => {
        const start = new Date();
        start.setDate(start.getDate() - 1);

        const params: TemperatureFilterParams = {
          start,
          north: profile.reverseGeocode.northBound,
          east: profile.reverseGeocode.eastBound,
          south: profile.reverseGeocode.southBound,
          west: profile.reverseGeocode.westBound
        };

        return params;
      })
    )),
    map(([, params]) => MapActions.loadTemperatures({ params }))
  ));

  loadTemperatures$ = createEffect(() => this.actions$.pipe(
    ofType(MapActions.loadTemperatures),
    mergeMap(({ params }) => this.temperatureService.getTemperatures(params).pipe(
      map(temperatures => MapActions.loadTemperaturesSuccess({ temperatures }))
    ))
  ));

  toggleHeatMap$ = createEffect(() => this.actions$.pipe(
    ofType(MapActions.updateShowHeatMap),
    filter(({ showHeatMap }) => !showHeatMap),
    withLatestFrom(this.store$.pipe(select(mapReducers.selectRange))),
    map(([, range]) => MapActions.updateTemperatureFilter({ range }))
  ));
}
