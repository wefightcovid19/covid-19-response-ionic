export interface SymptomDto {
  id: string;
  name: string;
}
