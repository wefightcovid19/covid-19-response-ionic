import { createAction, props } from '@ngrx/store';
import { SymptomModel } from '../../../../models/symptom.model';
import { TemperatureType } from '../../../../exports/symptoms.exports';

export const selectSymption = createAction('[Report Symptoms] Select Symptom', props<{ symptom: SymptomModel; }>());
export const deselectSymption = createAction('[Report Symptoms] Deselect Symptom', props<{ symptom: SymptomModel; }>());
export const updateTemperature = createAction('[Report Symptoms] Update Temperature', props<{ temperature: number; }>());
export const updateTemperatureType = createAction(
  '[Report Symptoms] Update Temperature Type',
  props<{ temperatureType: TemperatureType; }>()
);
export const updateLocation = createAction('[Report Symptoms] Update Location', props<{ latitude: number; longitude: number; }>());
export const alertSubmitSymptoms = createAction('[Report Symptoms] Alert Submit Symptoms');
export const submitSymptoms = createAction('[Report Symptoms] Submit Symptoms');
export const submitSymptomsSuccess = createAction('[Report Symptoms] Submit Symptoms Success');
export const reset = createAction('[Report Symptoms] Reset');
