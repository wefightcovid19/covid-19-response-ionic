import { Component, ViewChild, Input } from '@angular/core';
import { TemperatureModel } from '../../../../../models/temperature.model';
import { PlaceModel } from '../../../../../models/place.model';

@Component({
  selector: 'app-heat-map',
  templateUrl: './heat-map.component.html',
  styleUrls: ['./heat-map.component.scss'],
})
export class HeatMapComponent {

  @ViewChild('map', { static: true })
  mapElement: any;

  map: google.maps.Map;

  @Input()
  set bounds(bounds: PlaceModel) {
    if (bounds) {
      const mapProperties: google.maps.MapOptions = {
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.HYBRID
      };

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
      this.map.fitBounds({
        north: bounds.northBound,
        east: bounds.eastBound,
        west: bounds.westBound,
        south: bounds.southBound
      });
    }
  }

  @Input()
  set temperatures(temperatures: TemperatureModel[]) {
    if (temperatures) {
      const heatmapData = temperatures.map(({ temperature, latitude, longitude }) => {
        const location = new google.maps.LatLng(latitude, longitude);
        const weightedLocation: google.maps.visualization.WeightedLocation = { location, weight: temperature };

        return weightedLocation;
      });

      const heatmap = new google.maps.visualization.HeatmapLayer({
        data: heatmapData,
        dissipating: true
      });

      heatmap.setMap(this.map);
    }
  }
}
