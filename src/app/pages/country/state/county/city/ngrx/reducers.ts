import { Action, createReducer, on, createFeatureSelector, createSelector } from '@ngrx/store';
import { PlaceModel } from '../../../../../../models/place.model';
import * as CityActions from './actions';

export const featureKey = 'city';
export const initialState: State = {};

export interface State {
  city?: PlaceModel;
  neighborhoods?: PlaceModel[];
}

const stateReducer = createReducer(
  initialState,
  on(CityActions.loadCitySuccess, (state, { city }) => ({ ...state, city })),
  on(CityActions.loadCitySymptomMetricSuccess, (state, { city, metric }) => ({ ...state, city: { ...city, metric } })),
  on(CityActions.loadNeighborhoodsSuccess, (state, { neighborhoods }) => ({ ...state, neighborhoods })),
  on(CityActions.loadSymptomMetricSuccess, (state, { neighborhood, metric }) => {
    const neighborhoods = [...state.neighborhoods];
    const index = neighborhoods.findIndex(n => n.placeId === neighborhood.placeId);

    if (index !== -1) {
      neighborhoods[index] = { ...neighborhood, metric };
    }

    return { ...state, neighborhoods };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectCityFeature = createFeatureSelector<State>(featureKey);
export const selectCity = createSelector(
  selectCityFeature,
  (state: State) => state.city
);
export const selectNeighborhoods = createSelector(
  selectCityFeature,
  (state: State) => state.neighborhoods
);
