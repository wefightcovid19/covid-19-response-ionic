export enum TemperatureType {
  FAHRENHEIT = 'fahrenheit',
  CELCIUS = 'celcius'
}

export const NO_SYMPTOMS = '83ac6b76-33bb-4702-be4e-a501319802fc';
export const TEMPERATURE_SYMPTOM = 'ebc5b106-fd6c-44c9-9180-6e3165b6241f';
