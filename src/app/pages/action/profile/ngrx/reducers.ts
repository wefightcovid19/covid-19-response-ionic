import { Action, createFeatureSelector, createSelector, on, createReducer } from '@ngrx/store';
import { ProfileModel } from '../../../../models/profile.model';
import * as Actions from './actions';

export const featureKey = 'updateProfile';
const initialState: State = {};

export interface State {
  profile?: ProfileModel;
}

const stateReducer = createReducer(
  initialState,
  on(Actions.updateProfileSuccess, Actions.updateProfileInitialize, (state, { profile }) => ({ ...state, profile })),
  on(Actions.updateProfileTemperatureUnit, (state, { temperatureUnit }) => ({
    ...state,
    profile: {
      ...state.profile,
      temperatureUnit
    }
  })),
  on(Actions.updateProfileGender, (state, { gender }) => ({ ...state, profile: { ...state.profile, gender } })),
  on(Actions.updateProfileAge, (state, { age }) => ({ ...state, profile: { ...state.profile, age } })),
  on(Actions.updateProfileLocation, (state, { profile }) => ({ ...state, profile })),
  on(Actions.updateProfilePossibleLocations, (state, { coords: { latitude, longitude } }) => ({
    ...state,
    profile: { ...state.profile, latitude, longitude }
  })),
  on(Actions.reset, () => ({ ...initialState }))
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

const selectProfileFeature = createFeatureSelector<State>(featureKey);
export const selectProfile = createSelector(
  selectProfileFeature,
  (state: State) => state.profile
);
