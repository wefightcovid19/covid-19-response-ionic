import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { LoadingOptions } from '@ionic/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { withLatestFrom, tap } from 'rxjs/operators';
import * as ProcessingIndicatorActions from './actions';
import * as reducers from './reducers';

@Injectable()
export class ProcessingIndicatorEffects {

  private loading: HTMLIonLoadingElement;

  constructor(
    private actions$: Actions,
    private loadingController: LoadingController,
    private store$: Store<reducers.State>
  ) { }

  showLoading$ = createEffect(() => this.actions$.pipe(
    ofType(ProcessingIndicatorActions.showLoading),
    tap(async () => {
      const options: LoadingOptions = {
        message: 'Please wait...'
      };

      this.loading = await this.loadingController.create(options);
      this.loading.present();
      this.loading.onWillDismiss().then(() => {
        this.loading = null;
      });
    })
  ), { dispatch: false });

  closeModal$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProcessingIndicatorActions.removeWaitingFor,
      ProcessingIndicatorActions.reset
    ),
    withLatestFrom(this.store$.pipe(select(reducers.selectWaitFor))),
    tap(([, waitFor]) => {
      if (waitFor.length === 0 && this.loading) {
        this.loading.dismiss();
      }
    })
  ), { dispatch: false });
}
