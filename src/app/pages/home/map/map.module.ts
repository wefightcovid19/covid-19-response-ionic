import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MapPageRoutingModule } from './map-routing.module';
import { MapPage } from './map.page';
import { LocationsMapModule } from '../../../components/locations-map/locations-map.module';
import { TemperatureRangeComponent } from './components/temperature-range/temperature-range.component';
import { HeatMapComponent } from './components/heat-map/heat-map.component';
import { MapEffects } from './ngrx/effects';
import { featureKey, reducer } from './ngrx/reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationsMapModule,
    MapPageRoutingModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([MapEffects])
  ],
  declarations: [
    MapPage,
    TemperatureRangeComponent,
    HeatMapComponent
  ]
})
export class MapPageModule { }
