import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PlaceModel } from '../../../models/place.model';
import { SymptomModel } from '../../../models/symptom.model';
import { BadgeColorService } from '../../../services/badge-color.service';
import { TEMPERATURE_SYMPTOM } from '../../../exports/symptoms.exports';
import * as symptomReducers from '../../../ngrx/symptom/reducers';
import * as Actions from './ngrx/actions';
import * as reducers from './ngrx/reducers';

@Component({
  selector: 'app-state',
  templateUrl: './state.page.html',
  styleUrls: ['./state.page.scss'],
})
export class StatePage {

  readonly TEMPERATURE_SYMPTOM = TEMPERATURE_SYMPTOM;

  state$: Observable<PlaceModel>;
  counties$: Observable<PlaceModel[]>;
  symptoms$: Observable<SymptomModel[]>;
  selected$: Observable<SymptomModel>;

  constructor(
    private store$: Store<reducers.State>,
    private route: ActivatedRoute,
    public badgeColorService: BadgeColorService
  ) {
    this.state$ = this.store$.pipe(select(reducers.selectState));
    this.counties$ = this.store$.pipe(select(reducers.selectCounties));
    this.symptoms$ = this.store$.pipe(select(symptomReducers.selectSymptoms));
    this.selected$ = this.store$.pipe(select(symptomReducers.selectSelected));
  }

  ionViewWillEnter() {
    this.route.paramMap.subscribe(params => {
      const stateId = params.get('stateId');
      this.store$.dispatch(Actions.loadState({ stateId }));
      this.store$.dispatch(Actions.loadCounties({ stateId }));
    });
  }

  selectSymptom(symptom: SymptomModel) {
    this.store$.dispatch(Actions.selectSymptom({ symptom }));
  }
}
