import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';
import { PlaceService } from '../../../../services/http/place.service';
import { SymptomService } from '../../../../services/http/symptom.service';
import { TemperatureService, TemperatureFilterParams } from '../../../../services/http/temperature.service';
import { TEMPERATURE_SYMPTOM } from '../../../../exports/symptoms.exports';
import * as SymptomActions from '../../../../ngrx/symptom/actions';
import * as symptomReducers from '../../../../ngrx/symptom/reducers';
import * as StateActions from './actions';
import * as stateReducers from './reducers';

@Injectable({
  providedIn: 'root'
})
export class StateEffects {

  constructor(
    private store$: Store<stateReducers.State>,
    private actions$: Actions,
    private placeService: PlaceService,
    private temperatureService: TemperatureService,
    private symptomService: SymptomService
  ) { }

  loadState$ = createEffect(() => this.actions$.pipe(
    ofType(StateActions.loadState),
    map(action => action.stateId),
    mergeMap(stateId => this.placeService.place(stateId).pipe(
      map(state => StateActions.loadStateSuccess({ state }))
    ))
  ));

  loadCountySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(StateActions.loadStateSuccess),
    mergeMap(({ state }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: state.northBound,
        east: state.eastBound,
        south: state.southBound,
        west: state.westBound
      };

      return this.temperatureService.getAverageTemperature(params).pipe(
        map(metric => StateActions.loadStateSymptomMetricSuccess({ metric, state }))
      );
    })
  ));

  loadCounties$ = createEffect(() => this.actions$.pipe(
    ofType(StateActions.loadCounties),
    map(action => action.stateId),
    mergeMap(stateId => this.placeService.children(stateId).pipe(
      map(counties => StateActions.loadCountiesSuccess({ counties }))
    ))
  ));

  loadCountiesSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(StateActions.loadCountiesSuccess),
    withLatestFrom(this.store$.pipe(select(symptomReducers.selectSelected))),
    mergeMap(([{ counties }, symptom]) => counties.map(county => StateActions.loadSymptomMetric({ county, symptom})))
  ));

  loadSymptomMetric$ = createEffect(() => this.actions$.pipe(
    ofType(StateActions.loadSymptomMetric),
    mergeMap(({ county, symptom }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: county.northBound,
        east: county.eastBound,
        south: county.southBound,
        west: county.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => StateActions.loadSymptomMetricSuccess({ metric, county })));
    })
  ));

  selectSymptom$ = createEffect(() => this.actions$.pipe(
    ofType(StateActions.selectSymptom),
    tap(({ symptom }) => this.store$.dispatch(SymptomActions.selectSymptom({ selected: symptom }))),
    withLatestFrom(this.store$.pipe(select(stateReducers.selectCounties))),
    mergeMap(([{ symptom }, counties]) => forkJoin(counties.map(county => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: county.northBound,
        east: county.eastBound,
        south: county.southBound,
        west: county.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => StateActions.loadSymptomMetricSuccess({ metric, county })));
    }))),
    mergeMap(results => results)
  ));
}
