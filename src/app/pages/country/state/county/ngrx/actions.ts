import { createAction, props } from '@ngrx/store';
import { PlaceModel } from '../../../../../models/place.model';
import { SymptomModel } from '../../../../../models/symptom.model';

export const loadCounty = createAction('[County Page] Load County', props<{ countyId: string }>());
export const loadCountySuccess = createAction('[County Page] Load County Success', props<{ county: PlaceModel; }>());
export const loadCountySymptomMetricSuccess = createAction(
  '[County Page] Load County Symptom Metric Success',
  props<{ metric: number; county: PlaceModel; }>()
);
export const loadCities = createAction('[County Page] Load Cities', props<{ countyId: string }>());
export const loadCitiesSuccess = createAction('[County Page] Load Cities Success', props<{ cities: PlaceModel[]; }>());
export const loadSymptomMetric = createAction('[County Page] Load Symptom Metric', props<{ city: PlaceModel; symptom: SymptomModel; }>());
export const loadSymptomMetricSuccess = createAction(
  '[County Page] Load Symptom Metric Success',
  props<{ metric: number; city: PlaceModel; }>()
);
export const selectSymptom = createAction('[County Page] Select Symptom', props<{ symptom: SymptomModel }>());
