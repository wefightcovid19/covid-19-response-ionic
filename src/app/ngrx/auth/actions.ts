import { createAction, props } from '@ngrx/store';
import { ProfileModel } from '../../models/profile.model';

export const updateLoginEmail = createAction('[Auth] Update Email', props<{ email: string; }>());
export const updateLoginPassword = createAction('[Auth] Update Password', props<{ password: string; }>());
export const loadEmail = createAction('[Auth] Load User Profile Email', props<{ email: string; }>());
export const loadProfileSuccess = createAction('[Auth] Load User Profile Success', props<{ profile: ProfileModel; }>());
export const login = createAction('[Auth] Login');
export const loginSuccess = createAction('[Auth] Login Success', props<{ token: string; }>());
export const loginFailure = createAction('[Auth] Login Failure', props<{ error: any; }>());
export const refresh = createAction('[Auth] Refresh', props<{ redirect: boolean; }>());
export const refreshSuccess = createAction('[Auth] Refresh Success');
export const refreshFailure = createAction('[Auth] Refresh Failure', props<{ redirect: boolean; }>());
export const reset = createAction('[Auth] Reset');
