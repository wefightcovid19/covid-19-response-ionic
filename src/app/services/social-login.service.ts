import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Store } from '@ngrx/store';
import { timer } from 'rxjs';
import { environment } from '../../environments/environment';
import * as AuthActions from '../ngrx/auth/actions';
import * as authReducers from '../ngrx/auth/reducers';

declare var Keycloak: any;

@Injectable()
export class SocialLoginService {
  private static keycloak: any = {};
  private static initialize: Promise<any>;

  constructor(
    private storage: Storage,
    private navController: NavController,
    private store$: Store<authReducers.State>
  ) { }

  init(authUrl: string): Promise<any> {
    SocialLoginService.initialize = new Promise<any>((resolve, reject) => {
      const config = {
        url: authUrl,
        realm: 'covid-19-response',
        clientId: 'temperature-tracker',
        'ssl-required': 'external',
        'public-client': true
      };

      const keycloak = new Keycloak(config);
      keycloak.onAuthRefreshSuccess = async () => {
        await this.storage.set('accessToken', keycloak.token);
        await this.storage.set('accessTokenExpiration', keycloak.tokenParsed.exp * 1000);
        await this.storage.set('refreshToken', keycloak.refreshToken);
        await this.storage.set('refreshTokenExpiration', keycloak.refreshTokenParsed.exp * 1000);
        await this.storage.set('socialLogin', true);

        this.store$.dispatch(AuthActions.refreshSuccess());
      };
      keycloak.onAuthSuccess = async () => {
        await keycloak.onAuthRefreshSuccess();

        if (!keycloak.tokenParsed.profile_id) {
          this.navController.navigateForward('/action/profile');
        }
      };
      keycloak.onAuthLogout = async () => {
        await this.storage.remove('accessToken');
        await this.storage.remove('accessTokenExpiration');
        await this.storage.remove('refreshToken');
        await this.storage.remove('refreshTokenExpiration');
        await this.storage.remove('socialLogin');

        this.navController.navigateForward('/', { replaceUrl: true });
      };
      keycloak.onAuthError = keycloak.onAuthLogout;
      keycloak.onAuthRefreshError = keycloak.onAuthLogout;
      keycloak.init({ onLoad: 'check-sso', flow: 'standard' }).success(() => {
        const logoutUrl = `{keycloak.authServerUrl}/realms/{config.realm}/protocol/openid-connect/logout?redirect_uri=/`;

        SocialLoginService.keycloak.authz = keycloak;
        SocialLoginService.keycloak.logoutUrl = logoutUrl;
        SocialLoginService.initialize = undefined;

        resolve();
      }).error((err) => {
        reject(err);
      });
    });

    return SocialLoginService.initialize;
  }

  login(idpHint: string, redirectUri = environment.socialProviderRedirectUri): Promise<void> {
    return this.authz().then(authz => authz.login({ idpHint, redirectUri }));
  }

  logout(redirectUri = environment.socialProviderRedirectUri): Promise<void> {
    return this.authz().then(authz => authz.logout({ redirectUri }));
  }

  authz(): Promise<any> {
    if (SocialLoginService.keycloak.authz) {
      return Promise.resolve(SocialLoginService.keycloak.authz);
    } else if (SocialLoginService.initialize) {
      return SocialLoginService.initialize
        .then(() => timer(100).toPromise())
        .then(() => SocialLoginService.keycloak.authz);
    } else {
      return this.init(environment.authBasePath).then(() => SocialLoginService.keycloak.authz);
    }
  }
}
