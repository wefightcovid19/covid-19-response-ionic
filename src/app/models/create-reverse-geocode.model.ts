export class CreateReverseGeocodeModel {
  latitude: number;
  longitude: number;
}
