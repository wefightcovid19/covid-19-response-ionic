import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';
import { PlaceService } from '../../../../../services/http/place.service';
import { SymptomService } from '../../../../../services/http/symptom.service';
import { TemperatureService, TemperatureFilterParams } from '../../../../../services/http/temperature.service';
import { TEMPERATURE_SYMPTOM } from '../../../../../exports/symptoms.exports';
import * as SymptomActions from '../../../../../ngrx/symptom/actions';
import * as symptomReducers from '../../../../../ngrx/symptom/reducers';
import * as CountyActions from './actions';
import * as countyReducers from './reducers';

@Injectable({
  providedIn: 'root'
})
export class CountyEffects {

  constructor(
    private store$: Store<countyReducers.State>,
    private actions$: Actions,
    private placeService: PlaceService,
    private temperatureService: TemperatureService,
    private symptomService: SymptomService
  ) { }

  loadCounty$ = createEffect(() => this.actions$.pipe(
    ofType(CountyActions.loadCounty),
    map(action => action.countyId),
    mergeMap(countyId => this.placeService.place(countyId).pipe(
      map(county => CountyActions.loadCountySuccess({ county }))
    ))
  ));

  loadCountySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CountyActions.loadCountySuccess),
    mergeMap(({ county }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: county.northBound,
        east: county.eastBound,
        south: county.southBound,
        west: county.westBound
      };

      return this.temperatureService.getAverageTemperature(params).pipe(
        map(metric => CountyActions.loadCountySymptomMetricSuccess({ metric, county }))
      );
    })
  ));

  loadCitie$ = createEffect(() => this.actions$.pipe(
    ofType(CountyActions.loadCities),
    map(action => action.countyId),
    mergeMap(countyId => this.placeService.children(countyId).pipe(
      map(cities => CountyActions.loadCitiesSuccess({ cities }))
    ))
  ));

  loadCitiesSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CountyActions.loadCitiesSuccess),
    withLatestFrom(this.store$.pipe(select(symptomReducers.selectSelected))),
    mergeMap(([{ cities }, symptom]) => cities.map(city => CountyActions.loadSymptomMetric({ city, symptom })))
  ));

  loadSymptomMetric$ = createEffect(() => this.actions$.pipe(
    ofType(CountyActions.loadSymptomMetric),
    mergeMap(({ city, symptom }) => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: city.northBound,
        east: city.eastBound,
        south: city.southBound,
        west: city.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => CountyActions.loadSymptomMetricSuccess({ metric, city })));
    })
  ));

  selectSymptom$ = createEffect(() => this.actions$.pipe(
    ofType(CountyActions.selectSymptom),
    tap(({ symptom }) => this.store$.dispatch(SymptomActions.selectSymptom({ selected: symptom }))),
    withLatestFrom(this.store$.pipe(select(countyReducers.selectCities))),
    mergeMap(([{ symptom }, cities]) => forkJoin(cities.map(city => {
      const start = new Date();
      start.setDate(start.getDate() - 1);

      const params: TemperatureFilterParams = {
        start,
        north: city.northBound,
        east: city.eastBound,
        south: city.southBound,
        west: city.westBound
      };

      let invocation = this.temperatureService.getAverageTemperature(params);

      if (symptom && symptom.id !== TEMPERATURE_SYMPTOM) {
        invocation = this.symptomService.count(symptom, params);
      }

      return invocation.pipe(map(metric => CountyActions.loadSymptomMetricSuccess({ metric, city })));
    }))),
    mergeMap(results => results)
  ));
}
