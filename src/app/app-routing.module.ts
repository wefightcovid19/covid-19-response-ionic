import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { NotAuthenticatedGuard } from './guards/not-authenticated.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    canActivate: [AuthenticatedGuard],
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'symptom/state',
    canActivate: [AuthenticatedGuard],
    loadChildren: () => import('./pages/country/country.module').then( m => m.CountryPageModule)
  },
  {
    path: 'action/profile',
    canActivate: [AuthenticatedGuard],
    loadChildren: () => import('./pages/action/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'action/symptoms',
    canActivate: [AuthenticatedGuard],
    loadChildren: () => import('./pages/action/symptoms/symptoms.module').then( m => m.SymptomsPageModule)
  },
  {
    path: 'auth/login',
    canActivate: [NotAuthenticatedGuard],
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
