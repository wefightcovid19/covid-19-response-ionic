var aws = require('aws-sdk');
var async = require('async');
var fs = require('fs');
var path = require("path");
var moment = require('moment');
var cmdArgs = require('command-line-args');

var s3 = new aws.S3({ apiVersion: '2006-03-01' });

var optionDefinitions = [
  { name: 'bucket', type: String }
];
const cmdOptions = cmdArgs(optionDefinitions);

if (!cmdOptions.hasOwnProperty('bucket')) {
  console.log('USAGE: node aws.deploy.js --bucket <bucketname>');

  return;
}
var source = 'www';
var target = moment(new Date()).format('YYYYMMDDHHmmss');
var htmlPath = path.join(source, 'index.html');

fs.readFile(htmlPath, 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  var result = data
    .replace(/base href="\//g, `base href="/${target}/`);

  fs.writeFile(htmlPath, result, 'utf8', function (err) {
    if (err) return console.log(err);
    deployDirectoryRecursively(source, target);
  });
});

/**
 * Deploys the source directory to target s3 bucket 
 * 
 * @param sourcePath the source directory path
 */
function deployDirectoryRecursively(sourcePath) {
  var items = fs.readdirSync(sourcePath);

  async.map(items, function (itemName, callback) {
    var itemPath = path.join(sourcePath, itemName);

    if (fs.lstatSync(itemPath).isDirectory()) {
      deployDirectoryRecursively(itemPath);
    } else {
      var options = {
        Bucket: cmdOptions.bucket,
        Key: itemPath.replace(source, target),
        Body: fs.readFileSync(itemPath)
      };

      if (itemName.indexOf('.css') != -1) {
        options.ContentType = 'text/css';
      } else if (itemName.indexOf('.json') != -1) {
        options.ContentType = 'application/json';
      } else if (itemName.indexOf('.js') != -1) {
        options.ContentType = 'application/javascript';
      } else if (itemName.indexOf('.woff2') != -1) {
        options.ContentType = 'font/woff2';
      } else if (itemName.indexOf('.html') != -1) {
        options.ContentType = 'text/html';
      }

      s3.putObject(options, callback);
    }
  }, function (err, results) {
    if (err) console.error(err);
    console.log(results);
  });
}