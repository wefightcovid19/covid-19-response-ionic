import { ToastOptions } from '@ionic/core';

export const DEFAULT_TOAST_OPTIONS: ToastOptions = {
  duration: 5000,
  color: 'dark'
};
