import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ProfilePageRoutingModule } from './profile-routing.module';
import { DirectiveModule } from '../../../directives/directive.module';
import { ProfilePage } from './profile.page';
import { ProfileEffects } from './ngrx/effects';
import { reducer, featureKey } from './ngrx/reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DirectiveModule,
    ProfilePageRoutingModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([
      ProfileEffects
    ])
  ],
  declarations: [
    ProfilePage
  ],
  providers: [
    Geolocation
  ]
})
export class ProfilePageModule {}
