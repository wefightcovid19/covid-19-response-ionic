import { Component, ViewChild, OnDestroy } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { tap, debounceTime, withLatestFrom, filter } from 'rxjs/operators';
import { TemperatureModel } from '../../../models/temperature.model';
import { PlaceModel } from '../../../models/place.model';
import { LocationsMapComponent } from '../../../components/locations-map/locations-map.component';
import { TemperatureRange } from '../../../exports/temperature.exports';
import * as MapReducers from './ngrx/reducers';
import * as MapActions from './ngrx/actions';


@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnDestroy {

  @ViewChild(LocationsMapComponent, { static: false })
  locationsMap: LocationsMapComponent;

  reverseGeocode$: Observable<PlaceModel>;
  showHeatMap$: Observable<boolean>;
  temperatures$: Observable<TemperatureModel[]>;
  temperatureRange$: Observable<TemperatureRange>;

  subscription: Subscription;

  constructor(
    private store$: Store<MapReducers.State>,
    private actions$: Actions
  ) {
    this.reverseGeocode$ = this.store$.pipe(select(MapReducers.selectReverseGeocode));
    this.showHeatMap$ = this.store$.pipe(select(MapReducers.selectShowHeatMap));
    this.temperatures$ = this.store$.pipe(select(MapReducers.selectTemperatures));
    this.temperatureRange$ = this.store$.pipe(select(MapReducers.selectRange));
    this.subscription = this.actions$.pipe(
      ofType(MapActions.updateTemperatureFilter),
      withLatestFrom(this.showHeatMap$),
      filter(([, showHeatMap]) => !showHeatMap),
      debounceTime(500),
      tap(([{ range }]) => this.locationsMap.updateFilteredTemperatures(range))
    ).subscribe();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ionViewWillEnter() {
    this.store$.dispatch(MapActions.initialize());
  }

  toggleShowHeatMap(showHeatMap: boolean) {
    this.store$.dispatch(MapActions.updateShowHeatMap({ showHeatMap }));
  }
}
