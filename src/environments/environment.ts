export const environment = {
  production: false,
  name: 'development',
  authBasePath: 'http://localhost/auth',
  temperatureTrackerBasePath: 'http://localhost/temperature-tracker',
  socialProviderRedirectUri: 'http://localhost:4200/home/map'
};
