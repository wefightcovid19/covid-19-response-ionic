import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Store } from '@ngrx/store';
import { Observable, from, EMPTY } from 'rxjs';
import { flatMap, switchMap } from 'rxjs/operators';
import { RefreshService } from '../../refresh.service';
import { AppState } from '../../../ngrx/reducers';
import * as AuthActions from '../../../ngrx/auth/actions';

@Injectable({ providedIn: 'root' })
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private store$: Store<AppState>,
    private storage: Storage,
    private refreshService: RefreshService,
    private platform: Platform
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        'Ionic-Platforms': this.platform.platforms().join()
      }
    });

    return from(this.storage.get('accessTokenExpiration')).pipe(
      flatMap(expiration => {
        let result: Observable<HttpEvent<any>> = next.handle(request);
        const authorizationHeaderRequired = !request.url.includes('/protocol/openid-connect/token')
          && !request.url.includes('/protocol/openid-connect/logout');

        if (expiration && authorizationHeaderRequired) {
          if (Date.now() > expiration) {
            result = this.refresh(request, next);
          } else {
            result = this.sendAuthorizedRequest(request, next);
          }
        }

        return result;
      })
    );
  }

  private refresh(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return from(this.storage.get('refreshTokenExpiration')).pipe(
      flatMap(expiration => {
        let result: Observable<HttpEvent<any>> = EMPTY;

        if (Date.now() < expiration) {
          result = this.refreshService.refresh().pipe(
            switchMap(refreshed => refreshed ? this.sendAuthorizedRequest(request, next) : EMPTY)
          );
        } else {
          this.store$.dispatch(AuthActions.refreshFailure({ redirect: true }));
        }

        return result;
      })
    );
  }

  private sendAuthorizedRequest(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return from(this.storage.get('accessToken')).pipe(
      flatMap(accessToken => {
        let result: Observable<HttpEvent<any>> = EMPTY;

        if (accessToken) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${accessToken}`,
            }
          });

          result = next.handle(request);
        }

        return result;
      })
    );
  }
}
