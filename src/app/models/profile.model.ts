import { GenderType } from '../exports/gender.export';
import { PlaceModel } from './place.model';
import { TemperatureType } from '../exports/symptoms.exports';

export class ProfileModel {

  age?: number;

  gender?: GenderType;

  temperatureUnit?: TemperatureType;

  reverseGeocode?: PlaceModel;

  latitude?: number;

  longitude?: number;
}
