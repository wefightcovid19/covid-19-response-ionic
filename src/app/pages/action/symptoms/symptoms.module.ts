import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SymptomsPageRoutingModule } from './symptoms-routing.module';
import { SymptomsPage } from './symptoms.page';
import { DirectiveModule } from '../../../directives/directive.module';
import { featureKey, reducer } from './ngrx/reducers';
import { SymptomsEffects } from './ngrx/effects';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DirectiveModule,
    SymptomsPageRoutingModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([
      SymptomsEffects
    ])
  ],
  declarations: [
    SymptomsPage
  ],
  providers: [
    Geolocation,
    DecimalPipe
  ]
})
export class SymptomsPageModule { }
