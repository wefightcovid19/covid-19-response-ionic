import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { IonInput } from '@ionic/angular';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take, tap, map, filter } from 'rxjs/operators';
import { SymptomModel } from '../../../models/symptom.model';
import * as authReducers from '../../../ngrx/auth/reducers';
import * as reducers from '../../../ngrx/symptom/reducers';
import * as Actions from './ngrx/actions';
import { TEMPERATURE_SYMPTOM } from '../../../exports/symptoms.exports';

@Component({
  selector: 'app-symptoms',
  templateUrl: './symptoms.page.html',
  styleUrls: ['./symptoms.page.scss'],
})
export class SymptomsPage implements OnInit {

  @ViewChild(IonInput, { static: false })
  input: IonInput;

  form: FormGroup;
  temperatureInputItemHeight: number;
  symptoms$: Observable<SymptomModel[]>;

  constructor(
    private store$: Store<reducers.State>,
    private formBuilder: FormBuilder
  ) {
    this.symptoms$ = this.store$.pipe(
      select(reducers.selectSymptoms),
      filter(symptoms => !!symptoms),
      map(symptoms => {
        const index = symptoms.findIndex(symptom => symptom.id === TEMPERATURE_SYMPTOM);
        if (index >= 0) {
          symptoms.splice(index, 1);
        }

        return symptoms;
      })
    ).pipe(
      tap(symptoms => {
        if (symptoms) {
          symptoms.forEach(symptom => {
            this.form.addControl(symptom.id, new FormControl(false));
          });
          this.form.setValidators((group: FormGroup): { [key: string]: any } => {
            let result = null;
            let noInput = group.controls.temperature.value.trim() === '';

            symptoms.forEach(symptom => {
              noInput = group.controls[symptom.id].value === false && noInput;
            });

            if (noInput) {
              result = { noInput };
            }

            return result;
          });
        }
      })
    );
    this.form = this.formBuilder.group({
      temperature: ['']
    }, {
      validators: (group: FormGroup): { [key: string]: any } => {
        return group.controls.temperature.value.trim() === '' ? { noInput: true } : null;
      }
    });
  }

  ngOnInit() {
    this.store$.pipe(
      select(authReducers.selectProfile),
      filter(profile => !!profile),
      tap(profile => this.store$.dispatch(Actions.updateTemperatureType({ temperatureType: profile.temperatureUnit }))),
      take(1)
    ).subscribe();
  }

  ionViewDidLeave() {
    this.store$.dispatch(Actions.reset());
  }

  temperatureUpdate(temperature: string) {
    this.store$.dispatch(Actions.updateTemperature({ temperature: Number.parseFloat(temperature) }));
  }

  toggleSymptom(checked: boolean, symptom: SymptomModel) {
    if (checked) {
      this.store$.dispatch(Actions.selectSymption({ symptom }));
    } else {
      this.store$.dispatch(Actions.deselectSymption({ symptom }));
    }
  }

  submitSymptoms() {
    this.store$.dispatch(Actions.alertSubmitSymptoms());
  }

  inputFocus() {
    this.input.setFocus();
  }

  setTemperatureInputItemHeight(height: number) {
    this.temperatureInputItemHeight = height;
  }
}
