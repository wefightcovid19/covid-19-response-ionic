import { Injectable } from '@angular/core';
import { ProfileModel } from '../models/profile.model';
import { CreateProfileDto } from '../dtos/create-profile.dto';
import { UpdateProfileDto } from '../dtos/update-profile.dto';
import { ProfileDto } from '../dtos/profile.dto';

@Injectable({
  providedIn: 'root'
})
export class ProfileAssembler {
  assembleCreateProfileDto(model: ProfileModel): CreateProfileDto {
    return {
      age: model.age,
      gender: model.gender,
      temperatureUnit: model.temperatureUnit,
      createReverseGeocode: {
        latitude: model.latitude,
        longitude: model.longitude
      }
    };
  }

  assembleUpdateProfileDto(model: ProfileModel): UpdateProfileDto {
    return {
      age: model.age,
      gender: model.gender,
      temperatureUnit: model.temperatureUnit,
      createReverseGeocode: {
        latitude: model.latitude,
        longitude: model.longitude
      }
    };
  }

  assembleProfileModel(dto: ProfileDto): ProfileModel {
    const model = new ProfileModel();
    model.age = dto.age;
    model.gender = dto.gender;
    model.temperatureUnit = dto.temperatureUnit;
    model.reverseGeocode = dto.reverseGeocode;

    return model;
  }
}
