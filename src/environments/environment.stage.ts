export const environment = {
  production: false,
  name: 'stage',
  authBasePath: 'https://stage.app.wefightcovid19.online/auth',
  temperatureTrackerBasePath: 'https://stage.app.wefightcovid19.online/temperature-tracker',
  socialProviderRedirectUri: 'https://stage.wefightcovid19.online/home/map'
};
