export class PlaceModel {
  placeId: string;
  formattedAddress: string;
  northBound: number;
  eastBound: number;
  southBound: number;
  westBound: number;
  latitude: number;
  longitude: number;
  metric?: number;
}
