export const environment = {
  production: false,
  name: 'test',
  authBasePath: 'https://test.app.wefightcovid19.online/auth',
  temperatureTrackerBasePath: 'https://test.app.wefightcovid19.online/temperature-tracker',
  socialProviderRedirectUri: 'https://test.wefightcovid19.online/home/map'
};
