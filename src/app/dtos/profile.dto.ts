import { GenderType } from '../exports/gender.export';
import { PlaceDto } from './place.dto';
import { TemperatureType } from '../exports/symptoms.exports';

export interface ProfileDto {
  gender: GenderType;
  age: number;
  temperatureUnit: TemperatureType;
  reverseGeocode: PlaceDto;
}
