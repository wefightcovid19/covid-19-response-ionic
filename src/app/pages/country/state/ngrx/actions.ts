import { createAction, props } from '@ngrx/store';
import { PlaceModel } from '../../../../models/place.model';
import { SymptomModel } from '../../../../models/symptom.model';

export const loadState = createAction('[State Page] Load State', props<{ stateId: string }>());
export const loadStateSuccess = createAction('[State Page] Load State Success', props<{ state: PlaceModel; }>());
export const loadStateSymptomMetricSuccess = createAction(
  '[State Page] Load State Symptom Metric Success',
  props<{ metric: number; state: PlaceModel; }>()
);
export const loadCounties = createAction('[State Page] Load Counties', props<{ stateId: string }>());
export const loadCountiesSuccess = createAction('[State Page] Load Counties Success', props<{ counties: PlaceModel[]; }>());
export const loadSymptomMetric = createAction('[State Page] Load Symptom Metric', props<{ county: PlaceModel; symptom: SymptomModel; }>());
export const loadSymptomMetricSuccess = createAction(
  '[State Page] Load Symptom Metric Success',
  props<{ metric: number; county: PlaceModel; }>()
);
export const selectSymptom = createAction('[State Page] Select Symptom', props<{ symptom: SymptomModel }>());
