import { TemperatureType } from '../exports/symptoms.exports';
import { CreateReverseGeocodeModel } from './create-reverse-geocode.model';
import { SymptomModel } from './symptom.model';

export class CreateTemperatureModel {
  temperature?: number;
  type?: TemperatureType;
  symptoms: SymptomModel[];
  createReverseGeocode?: CreateReverseGeocodeModel;
}
