import { Component, Input, ViewChild } from '@angular/core';
import { TemperatureModel } from '../../models/temperature.model';
import { SymptomModel } from '../../models/symptom.model';
import { PlaceModel } from '../../models/place.model';
import { TEMPERATURE_SYMPTOM } from '../../exports/symptoms.exports';
import { TemperatureRange } from '../../exports/temperature.exports';
import { } from 'googlemaps';

@Component({
  selector: 'app-locations-map',
  templateUrl: './locations-map.component.html',
  styleUrls: ['./locations-map.component.scss']
})
export class LocationsMapComponent {

  @ViewChild('map', { static: true })
  mapElement: any;

  @Input()
  temperatures: TemperatureModel[];

  @Input()
  symptom: SymptomModel;

  markers: google.maps.Marker[];
  map: google.maps.Map;

  @Input()
  set bounds(place: PlaceModel) {
    if (place) {
      const padding = -10;
      const center = new google.maps.LatLng(place.latitude, place.longitude);
      const northEast = new google.maps.LatLng(place.northBound, place.eastBound);
      const southWest = new google.maps.LatLng(place.southBound, place.westBound);
      const bounds = new google.maps.LatLngBounds(southWest, northEast);
      const mapProperties: google.maps.MapOptions = {
        mapTypeId: google.maps.MapTypeId.HYBRID,
        streetViewControl: false,
        mapTypeControl: false,
        zoomControl: false
      };

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
      this.map.setCenter(center);
      this.map.fitBounds(bounds, padding);
    }
  }

  @Input()
  set places(places: PlaceModel[]) {
    if (places && places[0].metric) {
      if (this.markers) {
        this.markers.forEach(marker => marker.setMap(null));
      }

      const getColor = (metric: number) => {
        let colorRanges = [
          { max: 100.4, color: 'LightGreen' },
          { min: 100.4, max: 101.4, color: 'Yellow' },
          { min: 101.4, color: 'Red' }
        ];

        if (this.symptom && this.symptom.id !== TEMPERATURE_SYMPTOM) {
          colorRanges = [
            { max: 10, color: 'LightGreen' },
            { min: 10, max: 100, color: 'Yellow' },
            { min: 100, color: 'Red' }
          ];
        }

        const { color } = colorRanges.find(r => (!r.min || metric >= r.min) && (!r.max || metric < r.max));
        return color;
      };

      this.markers = places
        .map(({ metric, latitude, longitude }) => {
          const color = getColor(metric);
          const icon: google.maps.ReadonlySymbol = {
            path: google.maps.SymbolPath.CIRCLE,
            fillOpacity: 1,
            fillColor: color,
            strokeColor: color,
            scale: 4
          };
          const position = new google.maps.LatLng(latitude, longitude);
          const marker = new google.maps.Marker({ position, icon });

          marker.setMap(this.map);

          return marker;
        });
    }
  }

  updateFilteredTemperatures(range: TemperatureRange) {
    if (this.temperatures) {
      if (this.markers) {
        this.markers.forEach(marker => marker.setMap(null));
      }

      const getColor = (temperature: number) => {
        const colorRanges = [
          { max: 100.4, color: 'LightGreen' },
          { min: 100.4, max: 101.4, color: 'Yellow' },
          { min: 101.4, color: 'Red' }
        ];
        const { color } = colorRanges.find(r => (!r.min || temperature >= r.min) && (!r.max || temperature < r.max));
        return color;
      };

      this.markers = this.temperatures
        .filter(({ temperature }) => temperature >= range.lower / 10 && temperature <= range.upper / 10)
        .map(({ temperature, latitude, longitude }) => {
          const color = getColor(temperature);
          const icon: google.maps.ReadonlySymbol = {
            path: google.maps.SymbolPath.CIRCLE,
            fillOpacity: 1,
            fillColor: color,
            strokeColor: color,
            scale: 4
          };
          const position = new google.maps.LatLng(latitude, longitude);
          const marker = new google.maps.Marker({ position, icon });

          marker.setMap(this.map);

          return marker;
        });
    }
  }
}
