import { createAction, props } from '@ngrx/store';
import { SymptomModel } from '../../models/symptom.model';

export const getSymptomsSuccess = createAction('[Symptoms] Get Symptoms Success', props<{ symptoms: SymptomModel[]; }>());
export const selectSymptom = createAction('[Symptoms] Select Symptom', props<{ selected: SymptomModel; }>());
