import { Injectable } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
import { of, Observable } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap, filter } from 'rxjs/operators';
import { AuthService } from '../../services/http/auth.service';
import { ProfileService } from '../../services/http/profile.service';
import { DEFAULT_TOAST_OPTIONS } from '../../exports/toast.exports';
import { AppState, selectUrl } from '../reducers';
import * as errorMessages from '../../exports/error-message.exports';
import * as reducers from './reducers';
import * as AuthActions from './actions';

@Injectable()
export class AuthEffects {

  constructor(
    private toastController: ToastController,
    private navController: NavController,
    private actions$: Actions,
    private store$: Store<AppState>,
    private authService: AuthService,
    private profileService: ProfileService,
    private storage: Storage
  ) { }

  login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.login),
    withLatestFrom(this.store$.pipe(select(reducers.selectLogin))),
    switchMap(([, model]) => this.authService.login(model).pipe(
      switchMap(token => this.authService.processToken(token)),
      map(token => AuthActions.loginSuccess({ token })),
      tap(() => this.storage.remove('socialLogin')),
      catchError(error => of(AuthActions.loginFailure({ error })))
    ))
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.loginSuccess),
    switchMap(() => this.profileService.getProfile().pipe(
      tap(profile => {
        this.storage.set('lastLogin', new Date());
        this.navController.navigateForward('/home/map');
        this.store$.dispatch(AuthActions.loadProfileSuccess({ profile }));
      }),
      catchError(() => this.navController.navigateForward('/action/profile'))
    ))
  ), { dispatch: false });

  loginFailure$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.loginFailure),
    tap(async ({ error }) => {
      if (error.error_description === errorMessages.ACCOUNT_DISABLED) {
        const options = DEFAULT_TOAST_OPTIONS;
        options.message = errorMessages.ACCOUNT_DISABLED;

        const toast = await this.toastController.create(options);
        toast.present();
      }
    }),
    switchMap(() => this.authService.clearToken())
  ), { dispatch: false });

  refresh$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.refresh),
    switchMap(({ redirect }) => this.storage.get('refreshToken').then(token => ({ redirect, token }))),
    withLatestFrom(this.store$.pipe(select(selectUrl), filter(url => url !== undefined))),
    switchMap(([{ redirect, token }]) => {
      let result: Observable<Action> = of(AuthActions.refreshFailure({ redirect }));

      if (token) {
        result = this.authService.refresh(token).pipe(
          switchMap(refreshedToken => this.authService.processToken(refreshedToken)),
          map(() => AuthActions.refreshSuccess()),
          catchError(() => of(AuthActions.refreshFailure({ redirect })))
        );
      }

      return result;
    })
  ));

  refreshSucces$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.refreshSuccess),
    switchMap(() => this.profileService.getProfile().pipe(
      map(profile => AuthActions.loadProfileSuccess({ profile }))
    ))
  ));

  refreshFailure$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.refreshFailure),
    tap(() => this.authService.clearToken()),
    tap(({ redirect }) => {
      if (redirect) {
        this.navController.navigateForward('/', { replaceUrl: true });
      }
    })
  ), { dispatch: false });
}
