import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Observable, from, of } from 'rxjs';
import { map, flatMap, tap } from 'rxjs/operators';
import { RefreshService } from '../services/refresh.service';

@Injectable({
  providedIn: 'root'
})
export class NotAuthenticatedGuard implements CanActivate {

  constructor(
    private storage: Storage,
    private refreshService: RefreshService,
    private navController: NavController
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return from(
      Promise.all([this.storage.get('accessTokenExpiration'), this.storage.get('refreshTokenExpiration')])
    ).pipe(
      flatMap(([accessTokenExpiration, refreshTokenExpiration]) => {
        const isAccessTokenExpired = Date.now() > accessTokenExpiration;
        const isRefreshTokenExpired = Date.now() > refreshTokenExpiration;
        let result = of(isAccessTokenExpired);

        if (isAccessTokenExpired && !isRefreshTokenExpired) {
          result = this.refreshService.refresh().pipe(
            map(refreshed => !refreshed)
          );
        }

        return result;
      }),
      tap(isExpired => {
        if (!isExpired) {
          this.navController.navigateForward('/home/map');
        }
      })
    );
  }
}
