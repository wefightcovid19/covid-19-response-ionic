import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ProcessingIndicatorEffects } from './effects';
import { reducer, featureKey } from './reducers';

@NgModule({
  imports: [
    IonicModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([
      ProcessingIndicatorEffects
    ])
  ]
})
export class ProcessingIndicatorModule { }
