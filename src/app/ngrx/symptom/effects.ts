import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';
import { SymptomService } from '../../services/http/symptom.service';
import * as SymptomsActions from './actions';
import * as AuthActions from '../auth/actions';

@Injectable()
export class SymptomEffects {

  constructor(
    private symptomService: SymptomService,
    private actions$: Actions
  ) { }

  getSymptoms$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.refreshSuccess, AuthActions.loginSuccess),
    switchMap(() => this.symptomService.symptoms().pipe(
      map(symptoms => SymptomsActions.getSymptomsSuccess({ symptoms }))
    ))
  ));
}
