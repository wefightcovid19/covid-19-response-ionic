import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Observable, from, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { RefreshService } from '../services/refresh.service';
import { SocialLoginService } from '../services/social-login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate, CanActivateChild {

  constructor(
    private storage: Storage,
    private refreshService: RefreshService,
    private socialLogin: SocialLoginService
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return from(
      this.socialLogin.authz().then(() => this.storage.get('accessTokenExpiration'))
    ).pipe(
      switchMap(expiration => {
        const isExpired = Date.now() > expiration;
        let result = of(!isExpired);

        if (isExpired && expiration) {
          result = this.refreshService.refresh();
        }

        return result;
      })
    );
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }
}
