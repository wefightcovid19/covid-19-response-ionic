import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Store } from '@ngrx/store';
import { Observable, defer } from 'rxjs';
import { LoginModel } from '../../models/login.model';
import { AppState } from '../../ngrx/reducers';
import { environment } from '../../../environments/environment';
import * as AuthActions from '../../ngrx/auth/actions';

const MILLIS_PER_SECOND = 1000;
const CLIENT_ID = 'temperature-tracker';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private refreshTimeout: any;

  constructor(
    private store$: Store<AppState>,
    private httpClient: HttpClient,
    private storage: Storage
  ) { }

  login(credentials: LoginModel): Observable<string> {
    const params = new HttpParams()
      .set('username', credentials ? credentials.email : '')
      .set('password', credentials ? credentials.password : '')
      .set('grant_type', 'password')
      .set('client_id', CLIENT_ID);

    return this.httpClient.post<string>(`${environment.authBasePath}/realms/covid-19-response/protocol/openid-connect/token`, params);
  }

  refresh(refreshToken: string): Observable<string> {
    const params = new HttpParams()
      .set('refresh_token', refreshToken)
      .set('grant_type', 'refresh_token')
      .set('client_id', CLIENT_ID);

    return this.httpClient.post<string>(`${environment.authBasePath}/realms/covid-19-response/protocol/openid-connect/token`, params);
  }

  logout(refreshToken: string): Observable<any> {
    const params = new HttpParams()
      .set('refresh_token', refreshToken)
      .set('grant_type', 'password')
      .set('client_id', CLIENT_ID);

    return this.httpClient.post(`${environment.authBasePath}/realms/covid-19-response/protocol/openid-connect/logout`, params);
  }

  clearToken(): Observable<any> {
    return defer(async () => {
      await this.storage.ready().then(async () => {
        await this.storage.remove('accessToken');
        await this.storage.remove('accessTokenExpiration');
        await this.storage.remove('refreshToken');
        await this.storage.remove('refreshTokenExpiration');
      });
    });
  }

  processToken(token: string): Observable<string> {
    const now = Date.now();

    clearTimeout(this.refreshTimeout);
    this.refreshTimeout = setTimeout(
      () => this.store$.dispatch(AuthActions.refreshFailure({ redirect: false })),
      token['refresh_expires_in'] * MILLIS_PER_SECOND
    );

    return defer(async () => {
      return await this.storage.ready().then(async () => {
        await this.storage.set('accessToken', token['access_token']);
        await this.storage.set('accessTokenExpiration', now + token['expires_in'] * MILLIS_PER_SECOND);
        await this.storage.set('refreshToken', token['refresh_token']);
        await this.storage.set('refreshTokenExpiration', now + token['refresh_expires_in'] * MILLIS_PER_SECOND);

        const decodedToken = this.decodeToken(token['access_token']);
        const email: string = decodedToken['email'];

        this.store$.dispatch(AuthActions.loadEmail({ email }));

        return token;
      });
    });
  }

  /*
  *
  * This code was copied from the following keycloak js adapter source:
  * https://github.com/keycloak/keycloak-js-bower/blob/be55743d4f52cb9c8d3f173a1e6b777cc318457c/dist/keycloak.js#L827
  *
  */
  private decodeToken(token: string): any {
    token = token.split('.')[1];

    token = token.replace('/-/g', '+');
    token = token.replace('/_/g', '/');
    switch (token.length % 4) {
      case 0:
        break;
      case 2:
        token += '==';
        break;
      case 3:
        token += '=';
        break;
      default:
        throw new Error('Invalid token');
    }

    token = (token + '===').slice(0, token.length + (token.length % 4));
    token = token.replace(/-/g, '+').replace(/_/g, '/');

    token = decodeURIComponent(escape(atob(token)));

    token = JSON.parse(token);

    return token;
  }
}
