import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BadgeColorService {

  temperature(temperature: number): string {
    let result = 'success';

    if (temperature > 102) {
      result = 'danger';
    } else if (temperature > 100) {
      result = 'warning';
    }

    return result;
  }
}
