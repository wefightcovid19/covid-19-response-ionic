import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SymptomSelectComponent } from './symptom-select.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    SymptomSelectComponent
  ],
  declarations: [
    SymptomSelectComponent
  ]
})
export class SymptomSelectModule { }
