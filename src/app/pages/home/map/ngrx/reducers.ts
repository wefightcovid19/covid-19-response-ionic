import { Action, createReducer, on, createFeatureSelector, createSelector } from '@ngrx/store';
import { TemperatureModel } from '../../../../models/temperature.model';
import { PlaceModel } from '../../../../models/place.model';
import { TemperatureRange } from '../../../../exports/temperature.exports';
import * as MapActions from './actions';

export const featureKey = 'map';
export const initialState: State = { showHeatMap: false };

export interface State {
  reverseGeocode?: PlaceModel;
  temperatures?: TemperatureModel[];
  showHeatMap: boolean;
  range?: TemperatureRange;
}

const stateReducer = createReducer(
  initialState,
  on(MapActions.updateReverseGeocode, (state, { reverseGeocode }) => ({ ...state, reverseGeocode })),
  on(MapActions.updateShowHeatMap, (state, { showHeatMap }) => ({ ...state, showHeatMap })),
  on(MapActions.loadTemperaturesSuccess, (state, { temperatures }) => ({ ...state, temperatures })),
  on(MapActions.updateTemperatureFilter, (state, { range }) => ({ ...state, range }))
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectMapFeature = createFeatureSelector<State>(featureKey);
export const selectTemperatures = createSelector(
  selectMapFeature,
  (state: State) => state.temperatures
);
export const selectShowHeatMap = createSelector(
  selectMapFeature,
  (state: State) => state.showHeatMap
);
export const selectRange = createSelector(
  selectMapFeature,
  (state: State) => state.range
);
export const selectReverseGeocode = createSelector(
  selectMapFeature,
  (state: State) => state.reverseGeocode
);
