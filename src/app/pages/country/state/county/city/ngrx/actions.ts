import { createAction, props } from '@ngrx/store';
import { PlaceModel } from '../../../../../../models/place.model';
import { SymptomModel } from '../../../../../../models/symptom.model';

export const loadCity = createAction('[City Page] Load City', props<{ cityId: string }>());
export const loadCitySuccess = createAction('[City Page] Load City Success', props<{ city: PlaceModel; }>());
export const loadCitySymptomMetricSuccess = createAction(
  '[City Page] Load City Symptom Metric Success',
  props<{ metric: number; city: PlaceModel; }>()
);
export const loadNeighborhoods = createAction('[City Page] Load Neighborhoods', props<{ cityId: string }>());
export const loadNeighborhoodsSuccess = createAction('[City Page] Load Neighborhoods Success', props<{ neighborhoods: PlaceModel[]; }>());
export const loadSymptomMetric = createAction(
  '[City Page] Load Symptom Metric',
  props<{ neighborhood: PlaceModel; symptom: SymptomModel; }>()
);
export const loadSymptomMetricSuccess = createAction(
  '[City Page] Load Symptom Metric Success',
  props<{ metric: number; neighborhood: PlaceModel; }>()
);
export const selectSymptom = createAction('[City Page] Select Symptom', props<{ symptom: SymptomModel }>());
