import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LocationsMapModule } from '../../../../components/locations-map/locations-map.module';
import { SymptomSelectModule } from '../../../../components/symptom-select/symptom-select.module';
import { CountyPageRoutingModule } from './county-routing.module';
import { CountyPage } from './county.page';
import { CountyEffects } from './ngrx/effects';
import { featureKey, reducer } from './ngrx/reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationsMapModule,
    SymptomSelectModule,
    CountyPageRoutingModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature([CountyEffects])
  ],
  declarations: [
    CountyPage
  ]
})
export class CountyPageModule { }
