import { TemperatureType } from '../exports/symptoms.exports';

export interface TemperatureDto {
  temperature: number;
  type: TemperatureType;
  latitude: number;
  longitude: number;
}
