import { Action, createSelector, createFeatureSelector, createReducer, on } from '@ngrx/store';
import * as Actions from './actions';

export const featureKey = 'processingIndicator';
export const initialState: State = { waitFor: [] };

export interface State {
  waitFor: string[];
}

const stateReducer = createReducer(
  initialState,
  on(Actions.showLoading, (state, { waitFor }) => ({ ...state, waitFor })),
  on(Actions.removeWaitingFor, (state, { actionType }) => {
    const waitFor = [...state.waitFor];
    const index = state.waitFor.indexOf(actionType);

    if (index !== -1) {
      waitFor.splice(index, 1);
    }

    return { ...state, waitFor };
  }),
  on(Actions.reset, () => ({ ...initialState }))
);

export function reducer(state: State | undefined, action: Action) {
  return stateReducer(state, action);
}

export const selectProcesingIndicator = createFeatureSelector<State>(featureKey);
export const selectWaitFor = createSelector(
  selectProcesingIndicator,
  (state: State) => state.waitFor
);
