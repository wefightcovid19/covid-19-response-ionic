import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs';
import { ProfileModel } from '../../models/profile.model';
import { environment } from '../../../environments/environment';
import { ProfileAssembler } from '../../assemblers/profile.assembler';
import { ProfileDto } from '../../dtos/profile.dto';

const url = `${environment.temperatureTrackerBasePath}/profile`;

@Injectable({ providedIn: 'root' })
export class ProfileService {

  constructor(
    private httpClient: HttpClient,
    private assembler: ProfileAssembler
  ) { }

  postProfile(profile: ProfileModel): Observable<ProfileModel> {
    const createDto = this.assembler.assembleCreateProfileDto(profile);
    return this.httpClient.post<ProfileDto>(url, createDto).pipe(
      map(dto => this.assembler.assembleProfileModel(dto))
    );
  }

  putProfile(profile: ProfileModel): Observable<ProfileModel> {
    const updateDto = this.assembler.assembleUpdateProfileDto(profile);
    return this.httpClient.put<ProfileDto>(url, updateDto).pipe(
      map(dto => this.assembler.assembleProfileModel(dto))
    );
  }

  getProfile(): Observable<ProfileModel> {
    return this.httpClient.get<ProfileDto>(url).pipe(
      map(dto => this.assembler.assembleProfileModel(dto))
    );
  }
}
