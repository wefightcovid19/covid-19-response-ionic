var aws = require('aws-sdk');
var cmdArgs = require('command-line-args');

var s3 = new aws.S3({ apiVersion: '2006-03-01' });

var optionDefinitions = [
  { name: 'bucket', type: String },
  { name: 'buildId', type: String },
  { name: 'cdnId', type: String }
];
const cmdOptions = cmdArgs(optionDefinitions);

if (!cmdOptions.hasOwnProperty('bucket') || !cmdOptions.hasOwnProperty('cdnId')) {
  console.log('USAGE: node aws.release.js --bucket <bucketname> --cdnId <cdnId> [--buildId <buildId>]');
  console.log('\nBuildId is optional argument, if buildId is not specified then the releases will be listed, otherwise');
  console.log('a release of the specified staged build will be deployed. The cdnId and bucket arguments are required.');

  return;
}

/**
 * Lists all the staged builds in the s3 bucket
 *
 * @param s3 aws service
 * @param bucket the name of the bucket to work with
 */
function listStagedBuilds(s3, bucket) {
  var params = {
    Bucket: bucket,
    Delimiter: '/'
  };

  s3.listObjects(params, function (err, data) {
    if (err) {
      console.log('There was an issue getting the list of deployments');
    } else {
      data.CommonPrefixes.forEach(function (prefix) {
        console.log(prefix.Prefix.substring(0, prefix.Prefix.length - 1));
      });
    }
  });
}

/**
 * Moves index.html from dist to root and invalidates the index.html with the CDN
 * for refresh
 * 
 * @param s3 aws service
 * @param bucket the name of the bucket to work with
 * @param cdnId the Id of the CloudFront CDN
 * @param buildId the Id of the build to release
 */
function copyObjectAndInvalidateCdn(s3, bucket, cdnId, buildId) {
  s3.copyObject({
    Bucket: bucket,
    CopySource: `${bucket}/${buildId}/index.html`,
    Key: 'index.html',
    ContentType: 'text/html',
    MetadataDirective: 'REPLACE'
  }, function (err, data) {
    if (err) {
      console.log(err, err.stack);
    } else {
      var cdn = new aws.CloudFront();

      var cdnParams = {
        DistributionId: cdnId,
        InvalidationBatch: {
          CallerReference: (Math.floor(Date.now() / 1000)).toString(),
          Paths: {
            Quantity: 1,
            Items: [
              '/index.html'
            ]
          }
        }
      };

      cdn.createInvalidation(cdnParams, function (err, data) {
        if (err) {
          console.log(err, err.stack);
        } else {
          console.log(buildId + ' has been deployed and the CDN invalidation has been triggered.');
        }
      });
    }
  });
}

// if build id is not set then show all the releases
if (!cmdOptions.hasOwnProperty('buildId')) {
  listStagedBuilds(s3, cmdOptions.bucket);
} else {
  copyObjectAndInvalidateCdn(s3, cmdOptions.bucket, cmdOptions.cdnId, cmdOptions.buildId);
}