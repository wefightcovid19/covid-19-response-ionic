import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { TemperatureFilterParams } from './temperature.service';
import { SymptomAssembler } from '../../assemblers/symptom.assembler';
import { SymptomDto } from '../../dtos/symptom.dto';
import { SymptomModel } from '../../models/symptom.model';

@Injectable({
  providedIn: 'root'
})
export class SymptomService {

  constructor(
    private httpClient: HttpClient,
    private symptomAssembler: SymptomAssembler
  ) { }

  symptoms(): Observable<SymptomModel[]> {
    return this.httpClient.get<SymptomDto[]>(`${environment.temperatureTrackerBasePath}/symptom`).pipe(
      map(dtos => dtos.map(dto => this.symptomAssembler.assembleSymptomDto(dto)))
    );
  }

  count(symptom: SymptomModel, filterParams: TemperatureFilterParams): Observable<number> {
    let params = new HttpParams()
      .append('start', filterParams.start.toISOString())
      .append('north', filterParams.north.toString())
      .append('east', filterParams.east.toString())
      .append('south', filterParams.south.toString())
      .append('west', filterParams.west.toString());

    if (filterParams.end) {
      params = params.append('end', filterParams.end.toISOString());
    }

    return this.httpClient.get<number>(`${environment.temperatureTrackerBasePath}/symptom/${symptom.id}/count`, { params });
  }
}
