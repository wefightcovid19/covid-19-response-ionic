import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SymptomModel } from '../../../../models/symptom.model';
import { PlaceModel } from '../../../../models/place.model';
import { BadgeColorService } from '../../../../services/badge-color.service';
import { TEMPERATURE_SYMPTOM } from '../../../../exports/symptoms.exports';
import * as symptomReducers from '../../../../ngrx/symptom/reducers';
import * as reducers from './ngrx/reducers';
import * as Actions from './ngrx/actions';

@Component({
  selector: 'app-county',
  templateUrl: './county.page.html',
  styleUrls: ['./county.page.scss'],
})
export class CountyPage {

  readonly TEMPERATURE_SYMPTOM = TEMPERATURE_SYMPTOM;

  county$: Observable<PlaceModel>;
  cities$: Observable<PlaceModel[]>;
  symptoms$: Observable<SymptomModel[]>;
  selected$: Observable<SymptomModel>;

  constructor(
    private store$: Store<reducers.State>,
    private route: ActivatedRoute,
    public badgeColorService: BadgeColorService
  ) {
    this.county$ = this.store$.pipe(select(reducers.selectCounty));
    this.cities$ = this.store$.pipe(select(reducers.selectCities));
    this.symptoms$ = this.store$.pipe(select(symptomReducers.selectSymptoms));
    this.selected$ = this.store$.pipe(select(symptomReducers.selectSelected));
  }

  ionViewWillEnter() {
    this.route.paramMap.subscribe(params => {
      const countyId = params.get('countyId');
      this.store$.dispatch(Actions.loadCounty({ countyId }));
      this.store$.dispatch(Actions.loadCities({ countyId }));
    });
  }

  selectSymptom(symptom: SymptomModel) {
    this.store$.dispatch(Actions.selectSymptom({ symptom }));
  }
}
