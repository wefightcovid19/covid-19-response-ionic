import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TemperatureModel } from '../../models/temperature.model';
import { TemperatureAssembler } from '../../assemblers/temperature.assembler';
import { TemperatureDto } from '../../dtos/temperature.dto.';
import { environment } from '../../../environments/environment';
import { CreateTemperatureModel } from '../../models/create-temperature.model';

export interface TemperatureFilterParams {
  start: Date;
  end?: Date;
  north: number;
  east: number;
  south: number;
  west: number;
}

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {

  constructor(
    private httpClient: HttpClient,
    private assembler: TemperatureAssembler
  ) { }

  postTemperature(temperature: CreateTemperatureModel): Observable<TemperatureModel> {
    const dto = this.assembler.assembleCreateTemperatureDto(temperature);
    return this.httpClient.post<TemperatureDto>(`${environment.temperatureTrackerBasePath}/temperature`, dto).pipe(
      map(this.assembler.assembleTemperatureModel)
    );
  }

  getTemperatures(filterParams: TemperatureFilterParams): Observable<TemperatureModel[]> {
    let params = new HttpParams()
      .append('start', filterParams.start.toISOString())
      .append('north', filterParams.north.toString())
      .append('east', filterParams.east.toString())
      .append('south', filterParams.south.toString())
      .append('west', filterParams.west.toString());

    if (filterParams.end) {
      params = params.append('end', filterParams.end.toISOString());
    }

    return this.httpClient.get<TemperatureDto[]>(`${environment.temperatureTrackerBasePath}/temperature`, { params }).pipe(
      map(dtos => dtos.map(this.assembler.assembleTemperatureModel))
    );
  }

  getAverageTemperature(filterParams: TemperatureFilterParams): Observable<number> {
    let params = new HttpParams()
      .append('start', filterParams.start.toISOString())
      .append('north', filterParams.north.toString())
      .append('east', filterParams.east.toString())
      .append('south', filterParams.south.toString())
      .append('west', filterParams.west.toString());

    if (filterParams.end) {
      params = params.append('end', filterParams.end.toISOString());
    }

    return this.httpClient.get<number>(`${environment.temperatureTrackerBasePath}/temperature/average`, { params });
  }
}
