import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Actions, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { appVersion, appCommitHash } from './app.version';
import { SocialLoginService } from './services/social-login.service';
import { RefreshService } from './services/refresh.service';
import { TEMPERATURE_SYMPTOM } from './exports/symptoms.exports';
import * as rootReducers from './ngrx/reducers';
import * as authReducers from './ngrx/auth/reducers';
import * as SymptomActions from './ngrx/symptom/actions';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  readonly environmentName: string = environment.name;
  readonly appVersion: string = appVersion;
  readonly appCommitHash: string = appCommitHash;

  splitPaneDisabled$: Observable<boolean>;
  email$: Observable<string>;

  selectedIndex = 0;
  appPages = [
    { title: 'Home', url: '/home/map', icon: 'home' },
    { title: 'Symptoms', url: '/symptom/state', icon: 'list' }
  ];

  constructor(
    private store$: Store<rootReducers.AppState>,
    private actions$: Actions,
    private platform: Platform,
    private storage: Storage,
    private refreshService: RefreshService,
    private socialLoginService: SocialLoginService
  ) {
    this.email$ = this.store$.pipe(select(authReducers.selectEmail));
    this.splitPaneDisabled$ = this.store$.pipe(
      select(rootReducers.selectUrl),
      map(url => url && url.indexOf('auth') !== -1)
    );
    this.initializeApp();
  }

  async initializeApp() {
    await this.platform.ready();

    this.actions$.pipe(ofType(ROUTER_NAVIGATION), take(1)).subscribe(async () => {
      this.socialLoginService.authz();

      const token = await this.storage.get('refreshToken');
      const path = window.location.pathname;

      if (token) {
        this.refreshService.refresh();
      }

      if (path !== undefined) {
        this.selectedIndex = this.appPages.findIndex(page => page.url.toLowerCase() === path.toLowerCase());
      }

      this.actions$.pipe(
        ofType(SymptomActions.getSymptomsSuccess),
        take(1),
        tap(({ symptoms }) => {
          const index = symptoms.findIndex(s => s.id === TEMPERATURE_SYMPTOM);
          const selected = symptoms[index];
          this.store$.dispatch(SymptomActions.selectSymptom({ selected }));
        })
      ).subscribe();
    });
  }
}
