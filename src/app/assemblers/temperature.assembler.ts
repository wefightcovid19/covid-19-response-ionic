import { Injectable } from '@angular/core';
import { TemperatureDto } from '../dtos/temperature.dto.';
import { TemperatureModel } from '../models/temperature.model';
import { CreateTemperatureModel } from '../models/create-temperature.model';
import { CreateTemperatureDto } from '../dtos/create-temperature.dto.';

@Injectable({
  providedIn: 'root'
})
export class TemperatureAssembler {

  assembleTemperatureModel(dto: TemperatureDto): TemperatureModel {
    const model = new TemperatureModel();
    model.latitude = dto.latitude;
    model.longitude = dto.longitude;
    model.temperature = dto.temperature;
    model.type = dto.type;

    return model;
  }

  assembleCreateTemperatureDto(model: CreateTemperatureModel): CreateTemperatureDto {
    return { ...model };
  }
}
