import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from '../../../../environments/environment';
import { appVersion, appCommitHash } from '../../../app.version';
import { SocialLoginService } from '../../../services/social-login.service';
import { AppState } from '../../../ngrx/reducers';
import * as AuthActions from '../../../ngrx/auth/actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  readonly environmentName = environment.name;
  readonly isDevelopment = environment.name === 'development' || window.location.href.includes('localhost');
  readonly appVersion: string = appVersion;
  readonly appCommitHash: string = appCommitHash;

  constructor(
    private store$: Store<AppState>,
    private socialLoginService: SocialLoginService
  ) { }

  googleLogin() {
    this.socialLoginService.login('google');
  }

  developmentLogin() {
    const email = 'test.user@covid19response.online';
    const password = 'VQtPTdbmT46FvmTVwq';

    this.store$.dispatch(AuthActions.updateLoginEmail({ email }));
    this.store$.dispatch(AuthActions.updateLoginPassword({ password }));
    this.store$.dispatch(AuthActions.login());
  }
}
