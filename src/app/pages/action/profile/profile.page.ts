import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap, filter } from 'rxjs/operators';
import { ProfileModel } from '../../../models/profile.model';
import { GenderType } from '../../../exports/gender.export';
import { TemperatureType } from '../../../exports/symptoms.exports';
import * as reducers from './ngrx/reducers';
import * as authReducers from '../../../ngrx/auth/reducers';
import * as Actions from './ngrx/actions';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {

  form: FormGroup;

  authProfile$: Observable<ProfileModel>;
  profile$: Observable<ProfileModel>;
  genderType = GenderType;
  temperatureType = TemperatureType;

  constructor(
    private store$: Store<reducers.State>,
    private formBuilder: FormBuilder
  ) {
    this.authProfile$ = this.store$.select(authReducers.selectProfile);
    this.profile$ = this.store$.select(reducers.selectProfile).pipe(
      filter(profile => !!profile),
      tap(profile => {
        const ageControl = this.form.get('age');
        ageControl.setValue(profile.age);
        ageControl.updateValueAndValidity();

        const genderControl = this.form.get('gender');
        genderControl.setValue(profile.gender);
        genderControl.updateValueAndValidity();

        const temperatureTypeControl = this.form.get('temperatureType');
        temperatureTypeControl.setValue(profile.temperatureUnit);
        temperatureTypeControl.updateValueAndValidity();

        if (profile.reverseGeocode) {
          const locationControl = this.form.get('location');
          locationControl.setValue(profile.reverseGeocode.formattedAddress);
          locationControl.updateValueAndValidity();
        }
      })
    );

    this.form = this.formBuilder.group({
      age: [null, [Validators.required]],
      gender: [null, [Validators.required]],
      temperatureType: [null, [Validators.required]],
      location: [null, [Validators.required]]
    });
  }

  ionViewDidLeave() {
    this.store$.dispatch(Actions.reset());
  }

  ionViewWillEnter() {
    this.store$.dispatch(Actions.initialize());
  }

  genderUpdate(gender: GenderType) {
    this.store$.dispatch(Actions.updateProfileGender({ gender }));
  }

  temperatureTypeUpdate(temperatureUnit: TemperatureType) {
    this.store$.dispatch(Actions.updateProfileTemperatureUnit({ temperatureUnit }));
  }

  ageUpdate(age: number) {
    this.store$.dispatch(Actions.updateProfileAge({ age }));
  }

  locationUpdate() {
    this.store$.dispatch(Actions.alertUpdateProfileLocation());
  }

  update() {
    this.store$.dispatch(Actions.updateProfile());
  }
}
